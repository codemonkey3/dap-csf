<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

 // get ACF
$hero_title = get_field('field_619a592b5c2b2');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="mb-10 entry-header">
		<?php
			if ( is_single() ) {
				?>
				<h1 class="mb-0 font-bold text-3xl lg:text-4xl text-blue entry-title">
					<?php echo $hero_title ? $hero_title : get_the_title(); ?>
				</h1>
				<?php
			} 
			else {
				?>
					<h2 class="text-blue entry-title">
						<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
							<?php echo $hero_title ? $hero_title : get_the_title(); ?>
						</a>
					</h2>
				<?php
			}
			
		if ( 'post' === get_post_type() ) { ?>
			<div class="text-sm text-black entry-meta">
				<?php dap_csf_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php } ?>
	</header><!-- .entry-header -->
	
	<div class="font-light entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'dap-csf' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
				
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dap-csf' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dap_csf_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
