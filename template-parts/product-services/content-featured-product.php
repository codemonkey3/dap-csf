<?php
  // featured product
  $featured_product_id = get_field('field_619cede818980');

  $intro_heading = get_field('field_61965e77d5e53', $featured_product_id);
  $intro_excerpt = get_field('field_61965e86d5e55', $featured_product_id);
?>

<div class="relative featured-product">
        
  <div class="featured-product-silder">
    <?php
      // Check rows exists.
      if( have_rows('field_61965e3df76fe', $featured_product_id) ):
        // Loop through rows.
        while( have_rows('field_61965e3df76fe', $featured_product_id) ) : the_row();

          // Load sub field value.
          $hero_bg = get_sub_field('field_61965e4bf76ff');
          ?>
            <div>
              <img class="object-fit w-full" src="<?php echo $hero_bg['url']; ?>" alt="<?php echo $hero_bg['alt']; ?>">
            </div>
          <?php
        endwhile;
      endif;
    ?>
  </div>
  
  <div class="lg:absolute left-0 bottom-12 mt-6 lg:mt-0 w-full">
    <div class="container">
      <div class="row">

        <div class="offset-lg-1 col-lg-10">
          <div class="bg-white featured-product-copy">
            
            <div>
              <p class="mb-4 font-light text-sm text-center lg:text-right text-blue text-uppercase">
                Products & services
              </p>

              <h2 class="mb-4 font-bold text-3xl lg:text-4xl text-blue text-uppercase">
                <?php echo ("" != $intro_heading) ? $intro_heading : get_the_title(); ?>
              </h2>
              <article>
                <?php echo wpautop( $intro_excerpt ); ?>
              </article>
              
              <div class="text-center">
                <a class="btn btn-ghost text-black" href="<?php echo esc_url( get_the_permalink($featured_product_id) ); ?>">
                  Learn more about the project
                </a>
              </div>
            </div>

          </div>
        </div>

      </div> 
    </div> <!-- .container -->
  </div>

</div>