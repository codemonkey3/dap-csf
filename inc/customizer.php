<?php
/**
 * dap-csf Theme Customizer.
 *
 * @package dap-csf
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function dap_csf_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    
    // Tracking Scripts
    $wp_customize->add_section( 'dap_csf_general_section' , array(
        'title'        => __('Tracking Scripts', 'dap-csf'),
        'priority'     => 99,
    ) );
    
    $wp_customize->add_setting( 'dap_csf_theme_options[analytics]', array(
        'default'       => '',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'analytics', array(
        'label'         => __( 'Analytics', 'dap-csf' ),
        'description'   => __( 'Paste your Google Analytics (or other) tracking code here.', 'dap-csf' ),
        'section'       => 'dap_csf_general_section',
        'settings'      => 'dap_csf_theme_options[analytics]',
        'type'          => 'textarea',
    ));

    $wp_customize->add_setting( 'dap_csf_theme_options[header_analytics]', array(
        'default'       => '',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'header_analytics', array(
        'label'         => __( 'Header Analytics', 'dap-csf' ),
        'description'   => __( 'NOTE: Use this box for analytics that require to be added early or in header area of your site pages, paste your Google Analytics (or other) tracking code here.', 'dap-csf' ),
        'section'       => 'dap_csf_general_section',
        'settings'      => 'dap_csf_theme_options[header_analytics]',
        'type'          => 'textarea',
    ));

    // Blog
    $wp_customize->add_section( 'dap_csf_blog_section' , array(
        'title'        => __('Blog', 'dap-csf'),
        'priority'     => 99,
    ) );
    
    $wp_customize->add_setting( 'dap_csf_theme_options[enable_post_comment]', array(
        'default'       => 'yes',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'dap_csf_enable_post_comment', array(
        'label'         => __( 'Enable Comment in Posts', 'dap-csf' ),
        'description'   => __( 'If check it will display comment form in posts.', 'dap-csf' ),
        'section'       => 'dap_csf_blog_section',
        'settings'      => 'dap_csf_theme_options[enable_post_comment]',
        'type'          => 'radio',
        'choices'       => array(
            'yes'       => __( 'Enable' ),
            'no'        => __( 'Disable' )
        )
    ));

    $wp_customize->add_setting( 'dap_csf_theme_options[enable_page_comment]', array(
        'default'       => 'yes',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'dap_csf_enable_page_comment', array(
        'label'         => __( 'Enable Comment in Pages', 'dap-csf' ),
        'description'   => __( 'If check it will display comment form in pages.', 'dap-csf' ),
        'section'       => 'dap_csf_blog_section',
        'settings'      => 'dap_csf_theme_options[enable_page_comment]',
        'type'           => 'radio',
        'choices'        => array(
            'yes'   => __( 'Enable' ),
            'no'    => __( 'Disable' )
        )
    ));
    
}
add_action( 'customize_register', 'dap_csf_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function dap_csf_customize_preview_js() {
    wp_enqueue_script( 'dap_csf_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'dap_csf_customize_preview_js' );
