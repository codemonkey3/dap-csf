<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package dap-csf
 */

/*
 * Remove default WP builtin actions
 */
remove_action( 'wp_head', 'wp_generator' );

function dap_csf_head_func() {
	?>
	<meta name="msapplication-tap-highlight" content="no">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	
	<?php
	// add prerender for speed
	if( 'post' == get_post_type() ) {
		$next_article_id = dap_csf_get_next_article_id();
		
		echo '<link rel="prerender" href="'. get_permalink( $next_article_id ) .'">';
	}
}
add_action( 'wp_head', 'dap_csf_head_func' );

/*
 * @desc Localize wp-ajax
 */
function dap_csf_init() {
	wp_enqueue_script( 'rsclean-request-script', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ) );
	wp_localize_script( 'rsclean-request-script', 'theme_ajax', array(
		'url'		=> admin_url( 'admin-ajax.php' ),
		'site_url' 	=> get_bloginfo('url'),
		'theme_url' => get_bloginfo('template_directory')
	) );
}
add_action( 'init', 'dap_csf_init' );


/**********************************
 * Help site speed
 **********************************/
if ( ! is_admin() ) {
  add_action( "wp_enqueue_scripts", "dap_csf_jquery_enqueue", 10 );
}

function dap_csf_jquery_enqueue() {
	wp_enqueue_style( 'dap-csf-style', get_stylesheet_uri() );
	
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', true );
	wp_enqueue_script( 'sticky-js', get_template_directory_uri() . '/js/sticky.min.js', false, '1.8.1', true );

	// slick js
	if( is_front_page() 
		|| is_category() 
		|| is_home() ) {
		wp_enqueue_script( 'slick-script-js', get_template_directory_uri() . '/js/slick.min.js', false, '1.8.1', true );
	}
	
	wp_enqueue_script( 'dap-csf-scripts', get_template_directory_uri() . '/js/scripts.js', ['jquery'], '1.0', true );
	wp_enqueue_script( 'dap-csf-lazysizes-scripts', get_template_directory_uri() . '/js/lazysizes.min.js', false, '1.0', true );
}

// Remove wp version param from any enqueued scripts
function dap_csf_remove_wp_ver( $src ) {
   	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
    	$src = remove_query_arg( 'ver', $src );
   	return $src;
}
add_filter( 'style_loader_src', 'dap_csf_remove_wp_ver', 9999 );
add_filter( 'script_loader_src', 'dap_csf_remove_wp_ver', 9999 );

function dap_csf_custom_footer() {
	ob_start();
	?>
		<script type="text/javascript">
			function downloadJSAtOnload() {
				var element = document.createElement("script");
				element.src = "<?php echo get_stylesheet_directory_uri(); ?>/js/defer.js";
				document.body.appendChild(element);
			}
			
			if (window.addEventListener)
				window.addEventListener("load", downloadJSAtOnload, false);
			else if (window.attachEvent)
				window.attachEvent("onload", downloadJSAtOnload);
			else window.onload = downloadJSAtOnload;
		</script>
	<?php
	echo ob_get_clean();
}
add_action( 'wp_footer', 'dap_csf_custom_footer' );

/**********************************
 * Customize WP Login
 **********************************/
function dap_csf_login_logo() 
{
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo 			= wp_get_attachment_image_src( $custom_logo_id , 'full' );
	?>
	<style type="text/css">
		.login h1 a {
			background: url('<?php echo esc_url( $logo[0] ) ?>') top center no-repeat;
		}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,500" rel="stylesheet">
	<?php 
}
add_action( 'login_head', 'dap_csf_login_logo' );

function rs_login_stylesheet() {
	wp_enqueue_style( 'rstheme-custom-login-css', get_stylesheet_directory_uri() . '/css/login-form.css' );
	wp_enqueue_script( 'rstheme-custom-login-js', get_stylesheet_directory_uri() . '/js/login-form.js' );
}
add_action( 'login_enqueue_scripts', 'rs_login_stylesheet' );

/**
 * ? Ignore sticky posts from home
 */
function swoop_ignore_sticky_from_home( $query ) {
	if ( ! is_admin() 
		&& $query->is_home() 
		&& $query->is_main_query() 
		|| $query->is_category() ) {
    
		// exclude post from the display
		$latest_post_id = get_latest_article_id();
		
		$query->set( 'post__not_in', [$latest_post_id] );
  }

	if( $title = $query->get( '_s' ) ) {
		add_filter( 'get_meta_sql', function( $sql ) use ( $title ) {
			global $wpdb;

			// Only run once:
			static $nr = 0;
			if( 0 != $nr++ ) return $sql;

			// Modified WHERE
			$sql['where'] = sprintf(
					" AND ( (%s OR %s) OR %s ) ",
					$wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
					$wpdb->prepare( "{$wpdb->posts}.post_content like '%%%s%%'", $title),
					mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
			);

			return $sql;
		});
	}
}
add_action( 'pre_get_posts', 'swoop_ignore_sticky_from_home' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}