<?php

function _get_post_ids( $post_type = 'post', $order = 'DESC' ) {

    // make sure the post-type exist
    if( ! post_type_exists( $post_type ) ) 
        return;
    
    $args = [
        'post_type'     => $post_type,
        'numberposts'   => 1,
        'order'         => $order,
        'fields'        => 'ids' // returned only the ID
    ];

    // get post_ids
    $post_ids = get_posts( $args )[0];

    // rest postdata
    wp_reset_postdata();
    
    return $post_ids;
}

/**
 * Get latest article post-type ID
 */
function get_latest_article_id() {
    return _get_post_ids( 'post' );
}

/**
 * Get oldest article post-type ID
 */
function get_oldest_article_id() {
    return _get_post_ids( 'post', 'ASC' );
}

/**
 * Get next article post-id
 */
function dap_csf_get_next_article_id() {
    if( is_single()
        && 'post' == get_post_type() ) {
        
        // get next post
        $next_post = get_next_post();
        
        if ( is_a( $next_post , 'WP_Post' ) ) {
            $loading_url    = get_permalink( $next_post->ID );
            $loading_title  = get_the_title( $next_post->ID );

            // set next latest-project ID
            $next_item_id    = $next_post->ID;
        } else {
            $loading_url        = get_permalink( get_oldest_article_id() );
            $loading_title      = get_the_title( get_oldest_article_id() );

            // set next latest-project ID
            $next_item_id       = get_oldest_article_id();
        }
        
        return $next_item_id;
    }

    return false;
}