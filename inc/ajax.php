<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Check if class already exist
if( ! class_exists('RS_Theme_Registration_AJAX')) :

/**
 * Main RS Theme AJAX Class
 *
 * @class Swoop_System_Hub
 * @version	1.0
 */

final class RS_Theme_Registration_AJAX {

    /**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
			add_action( 'wp_ajax_nopriv_give_deposit_order_request', [$this, 'rstheme_ajax_request_func'] );
			add_action( 'wp_ajax_give_deposit_order_request', [$this, 'rstheme_ajax_request_func'] );
    }

    /**
     *	@desc Request Function
     */
    public function rstheme_ajax_request_func() {
        
    }
}

// End if( ! class_exists('RS_Theme_Registration_AJAX'))
endif;

new RS_Theme_Registration_AJAX();