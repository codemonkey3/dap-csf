<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package dap-csf
 */

/**
 * Register a custom post type called "search grant".
 *
 * @see get_post_type_labels() for label keys.
 */
function dap_csf_register_post_type_init() {
    $post_types['project'] = [
        'labels'             => [
            'name'                  => _x( 'Projects', 'Post type general name', 'dap-csf' ),
            'singular_name'         => _x( 'Project', 'Post type singular name', 'dap-csf' ),
            'menu_name'             => _x( 'Projects', 'Admin Menu text', 'dap-csf' ),
            'name_admin_bar'        => _x( 'Project', 'Add New on Toolbar', 'dap-csf' ),
            'add_new'               => __( 'Add New', 'dap-csf' ),
            'add_new_item'          => __( 'Add New project', 'dap-csf' ),
            'new_item'              => __( 'New project', 'dap-csf' ),
            'edit_item'             => __( 'Edit project', 'dap-csf' ),
            'view_item'             => __( 'View project', 'dap-csf' ),
            'all_items'             => __( 'All projects', 'dap-csf' ),
            'search_items'          => __( 'Search projects', 'dap-csf' ),
            'parent_item_colon'     => __( 'Parent projects:', 'dap-csf' ),
            'not_found'             => __( 'No projects found.', 'dap-csf' ),
            'not_found_in_trash'    => __( 'No projects found in Trash.', 'dap-csf' ),
            'featured_image'        => _x( 'Project Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'dap-csf' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'dap-csf' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'dap-csf' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'dap-csf' ),
            'archives'              => _x( 'Project archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'dap-csf' ),
            'insert_into_item'      => _x( 'Insert into project', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'dap-csf' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this project', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'dap-csf' ),
            'filter_items_list'     => _x( 'Filter projects list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'dap-csf' ),
            'items_list_navigation' => _x( 'Projects list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'dap-csf' ),
            'items_list'            => _x( 'Projects list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'dap-csf' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => [ 'slug' => 'testimonial', 'with_front' => false ],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => [ 'title', 'editor', 'page-attributes' ],
        'menu_icon'          => 'dashicons-hammer'
    ];
    
    if( $post_types ) {
        // loop through all post-types
        foreach( $post_types as $post_type => $args ) {
            register_post_type( $post_type, $args );
        }
    }
}

add_action( 'init', 'dap_csf_register_post_type_init' );