<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package dap-csf
 */

if ( ! function_exists( 'dap_csf_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function dap_csf_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'dap-csf' ),
		'<a class="text-black" href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'dap-csf' ),
		'<span class="author vcard">
			<a class="url fn n text-black" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>
		</span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span>
		<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'dap_csf_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function dap_csf_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		?>
		<div class="flex align-items-center">
			<div>
				<div class="flex align-items-center">
					<svg class="mr-2" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M19.6754 7.41546L12.8003 1.47855C12.1985 0.958824 11.25 1.38074 11.25 2.18808V5.31515C4.97543 5.38699 0 6.64453 0 12.5909C0 14.9909 1.54613 17.3685 3.2552 18.6116C3.78852 18.9996 4.54859 18.5127 4.35195 17.8839C2.5807 12.2193 5.19207 10.7155 11.25 10.6283V14.0625C11.25 14.8711 12.1992 15.2911 12.8003 14.772L19.6754 8.83453C20.1079 8.46101 20.1085 7.78949 19.6754 7.41546Z" fill="black"/>
					</svg>
					<span class="mr-2">Share: </span>
				</div>
			</div>	
			<div>
				<div class="flex align-items-center">
					<a class="mr-1" target="_blank" 
						href="https://www.facebook.com/share.php?u=<?php echo esc_url( get_the_permalink() ); ?>"
						style="height: 20px;">
						<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M17 1H3C1.9 1 1 1.9 1 3V17C1 18.101 1.9 19 3 19H10V12H8V9.525H10V7.475C10 5.311 11.212 3.791 13.766 3.791L15.569 3.793V6.398H14.372C13.378 6.398 13 7.144 13 7.836V9.526H15.568L15 12H13V19H17C18.1 19 19 18.101 19 17V3C19 1.9 18.1 1 17 1Z" fill="black"/>
						</svg>
					</a>
					<a class="mr-1" target="_blank" 
						href="https://twitter.com/share?url=<?php echo get_the_permalink(); ?>&text=<?php echo get_the_title(); ?>"
						style="height: 24px;">
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M20.625 2.625H3.375C2.96016 2.625 2.625 2.96016 2.625 3.375V20.625C2.625 21.0398 2.96016 21.375 3.375 21.375H20.625C21.0398 21.375 21.375 21.0398 21.375 20.625V3.375C21.375 2.96016 21.0398 2.625 20.625 2.625ZM17.0461 9.41484C17.0531 9.525 17.0531 9.63984 17.0531 9.75234C17.0531 13.193 14.4328 17.1562 9.64453 17.1562C8.16797 17.1562 6.79922 16.7273 5.64609 15.9891C5.85703 16.0125 6.05859 16.0219 6.27422 16.0219C7.49297 16.0219 8.61328 15.6094 9.50625 14.9109C8.3625 14.8875 7.40156 14.1375 7.07344 13.1063C7.47422 13.1648 7.83516 13.1648 8.24766 13.0594C7.65873 12.9397 7.12939 12.6199 6.74957 12.1542C6.36974 11.6885 6.16286 11.1056 6.16406 10.5047V10.4719C6.50859 10.6664 6.91406 10.7859 7.33828 10.8023C6.98166 10.5647 6.6892 10.2427 6.48682 9.86491C6.28445 9.48715 6.17841 9.06528 6.17813 8.63672C6.17813 8.15156 6.30469 7.70859 6.53203 7.32422C7.18571 8.12891 8.0014 8.78705 8.92609 9.25586C9.85078 9.72466 10.8638 9.99364 11.8992 10.0453C11.5312 8.27578 12.8531 6.84375 14.4422 6.84375C15.1922 6.84375 15.8672 7.15781 16.343 7.66406C16.9312 7.55391 17.4937 7.33359 17.9953 7.03828C17.8008 7.64062 17.393 8.14922 16.8516 8.47031C17.3766 8.41406 17.8828 8.26875 18.3516 8.06484C17.9977 8.58516 17.5547 9.04688 17.0461 9.41484Z" fill="black"/>
						</svg>
					</a>
					<a class="mr-1" target="_blank" 
						href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink(); ?>&title=<?php echo get_the_title(); ?>"
						style="height: 20px;">
						<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M0 1.4325C0 0.64125 0.6575 0 1.46875 0H18.5312C19.3425 0 20 0.64125 20 1.4325V18.5675C20 19.3587 19.3425 20 18.5312 20H1.46875C0.6575 20 0 19.3587 0 18.5675V1.4325ZM6.17875 16.7425V7.71125H3.1775V16.7425H6.17875ZM4.67875 6.4775C5.725 6.4775 6.37625 5.785 6.37625 4.9175C6.3575 4.03125 5.72625 3.3575 4.69875 3.3575C3.67125 3.3575 3 4.0325 3 4.9175C3 5.785 3.65125 6.4775 4.65875 6.4775H4.67875ZM10.8138 16.7425V11.6988C10.8138 11.4288 10.8337 11.1587 10.9137 10.9662C11.13 10.4275 11.6238 9.86875 12.4538 9.86875C13.54 9.86875 13.9738 10.6962 13.9738 11.9113V16.7425H16.975V11.5625C16.975 8.7875 15.495 7.4975 13.52 7.4975C11.9275 7.4975 11.2137 8.3725 10.8138 8.98875V9.02H10.7938C10.8004 9.00957 10.8071 8.99915 10.8138 8.98875V7.71125H7.81375C7.85125 8.55875 7.81375 16.7425 7.81375 16.7425H10.8138Z" fill="black"/>
						</svg>
					</a>
				</div>
			</div>
		</div>
		<?php
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function dap_csf_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'dap_csf_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'dap_csf_categories', $all_the_cool_cats );
	}
	
	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so dap_csf_categorized_blog should return true.
		return true;
	} 
	else {
		// This blog has only 1 category so dap_csf_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in dap_csf_categorized_blog.
 */
function dap_csf_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'dap_csf_categories' );
}
add_action( 'edit_category', 'dap_csf_category_transient_flusher' );
add_action( 'save_post',     'dap_csf_category_transient_flusher' );


/**
 * @desc	Truncate text
 * @param 	$text Text to truncate
 * @param 	$amount	Amount of text to display
 */
function dap_csf_excerpt( $text, $amount = 100 ) {
	return substr( $text, 0, strrpos(substr( $text, 0, $amount ), ' ') );
}