<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();
?>

	<?php
		$featured_args = [
			'p' => get_latest_article_id(),
			'posts_per_page' => 1
		];
		
		// The Query
		$the_query = new WP_Query( $featured_args );
		
		// The Loop
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) { $the_query->the_post();

				$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
				?>
				<style>
					@media (min-width: 1024px) {
						.featured-post:before {
							background-image: url(<?php echo esc_url( $featured_img_url ); ?>);
						}
					}
				</style>

				<div class="relative bg-gray-15 featured-post">
					
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
					
								<div class="row">
									<div class="col-lg-6">
									
										<div class="block lg:hidden">
											<?php if ( has_post_thumbnail() ) : ?>
												<figure class="mb-0">
													<?php the_post_thumbnail('full'); ?>
												</figure>
											<?php endif; ?>
										</div>

									</div>
									<div class="col-lg-6">

										<div class="py-8">
											<header>
												<h2 class="mb-0 text-3xl lg:text-4xl"><?php the_title(); ?></h2>
											</header>

											<time class="mb-5 text-sm block" datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
												<?php echo get_the_date(); ?>
											</time>
											
											<div class="mb-8 font-light excerpt">
												<?php the_excerpt(); ?>
											</div>
											
											<a class="text-uppercase text-yellow" href="<?php the_permalink(); ?>">
												<div class="media align-items-center">
													<span class="mr-2">Read More</span>
													<span>❯</span>
												</div>
											</a>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>						
				</div>
			<?php
			}
		}

		/* Restore original Post Data */
		wp_reset_postdata();
	?>

	<main id="main" class="site-main" role="main">
		<div class="container">
			
			<div class="row">

      	<!--sidebar-->
				<?php get_sidebar(); ?>
				
				<div class="col-lg-9">
					<div id="primary" class="content-area">
						
						<?php if ( have_posts() ) { ?>
							
							<header class="pt-8 lg:pt-13 pb-9 text-center page-header">
								<h2 class="mb-0 text-xl lg:text-2xl">New Trends and Updates in</h2>
								<h1 class="font-bold text-xl lg:text-2xl text-blue">Category</h1>
							</header>
											
							<div class="mb-14">
								<?php while ( have_posts() ) { the_post(); ?>
									<article id="post-<?php the_ID(); ?>" class="p-4 mb-4 article-card bg-white">
										<div class="row">
											<div class="col-lg-4">
												<figure class="mb-6 lg:mb-0">
													<?php
														if ( has_post_thumbnail() ) {
															the_post_thumbnail();
														}
													?>
												</figure>
											</div>
											
											<div class="col-lg-8">
												<header class="entry-header">
													<?php
														the_title( '<h2 class="mb-1 font-bold text-lg lg:text-xl entry-title">', '</h2>' );
													?>

													<div class="mb-4 entry-meta">
														<?php echo get_the_author() ?>
													</div><!-- .entry-meta -->
												</header><!-- .entry-header -->
												
												<div>
													<article class="font-light">
														<?php the_excerpt(); ?>
													</article>

													<a class="text-black" href="<?php the_permalink(); ?>">
														<div class="flex align-items-center">
															<span class="mr-2">Read more</span>
															<span>❯</span>
														</div>
													</a>
												
												</div>
											</div>
										</div>
									</article>	
								<?php 
								} // End of the loop.
								
								/* Restore original Post Data */
								wp_reset_postdata();
								?>
							</div>
						
							<?php							
								if( have_posts() ) {
									the_posts_pagination([
										'class' => 'text-center',
										'mid_size'  => 4,
										'prev_text' => __( '<div class="inline-flex align-items-center"><span>❮</span> <span class="ml-2">Prev</span></div>', 'dap-csf' ),
										'next_text' => __( '<div class="inline-flex align-items-center"><span class="mr-2">Next</span> <span>❯</span></div>', 'dap-csf' ),
									]);
								}

							}

						else {
							get_template_part( 'template-parts/content', 'none' );
						}
					?>

					</div><!-- #primary -->
				</div>
				
			</div>

		</div> <!-- .container -->
	</main><!-- #main -->

<?php
get_footer();