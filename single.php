<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package james-walker
 */

get_header();

// get ACF
$hero_image = get_field('field_619a593f5c2b3');
?>
	
	<main id="main" class="site-main" role="main">

		<div class="mb-13 hero">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php
							if( $hero_image && "" != $hero_image['url'] ) {
								$hero_image_url = $hero_image['url'];
							}
							else {
								$hero_image_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
							}
						?>
						<img 
							src="<?php echo esc_url( $hero_image_url ); ?>"
							alt="<?php echo esc_attr( get_the_title() ); ?>"
							class="object-cover w-full">							
					</div>
				</div>
			</div>
		</div>
		
		<div id="primary" class="content-area">
			<div class="container">
				
				<div class="row">
					<div class="col-lg-12">

						<?php
							while ( have_posts() ) : the_post();
								
								get_template_part( 'template-parts/content', get_post_format() );
								
							endwhile; // End of the loop.
						?>
						
					</div>
				</div>

			</div> <!-- .container -->
		</div> <!-- #primary -->

	</main><!-- #main -->

<?php
get_footer();