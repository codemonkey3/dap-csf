<?php
/**
 * Template Name: Sitemap
 * 
 * The template for displaying sitemap pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header(); ?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="pb-17 intro-text">
        <div class="container">
          
          <div class="row">
            <div class="offset-lg-2 col-lg-8">
              
              <div class="text-center">
                <?php while ( have_posts() ) : the_post(); ?>
                  <header class="mb-4">
                    <h1 class="font-bold text-xl lg:text-2xl text-blue"><?php the_title(); ?></h1>
                  </header>
                <?php endwhile; // End of the loop. ?>
              </div>

            </div>
          </div>

        </div> <!-- .container -->
      </div> <!-- .intro-text -->

			<div class="pb-12 sitemap">
				<div class="container">
					
					<div class="row justify-content-center">
						<div class="col-md-10">

							<?php
								// Check rows exists.
								if( have_rows('field_619500bb0fe1d') ) {

									$item_counter = 1;
									?>

									<div class="row">
										<?php
										// Loop through rows.
										while( have_rows('field_619500bb0fe1d') ) { the_row();

											// Load sub field value.
											$parent_menu = get_sub_field('field_619500940fe1c');

											if( $item_counter % 2 == 0 ) {
												$item_class = 'col-sm-6 col-md-4 col-lg-3';
											}
											else {
												$item_class = 'col-sm-6 col-md-4 col-lg-2';
											}
											?>
											<div class="<?php echo $item_class; ?>">
												<div class="mb-6 lg:mb-0">
													<a class="lg:mb-10 text-md block parent-menu-item" href="<?php echo $parent_menu['url']; ?>">
														<?php echo $parent_menu['title']; ?>
													</a>
													
													<?php
														// Check rows exists.
														if( have_rows('field_619500ec0fe1f') ) {
															?>
															
															<ul class="mb-0 ml-0 pl-0 list-none">
																<?php
																	// Loop through rows.
																	while( have_rows('field_619500ec0fe1f') ) : the_row();

																		// Load sub field value.
																		$menu_item = get_sub_field('field_619501010fe20');
																		?>
																		<li>
																			<a class="font-light text-black block" href="<?php echo $menu_item['url']; ?>">
																				<?php echo $menu_item['title']; ?>
																			</a>
																		</li>
																		<?php
																	// End loop.
																	endwhile;
																	?>
																</ul>
															
															<?php
															
														}
													?>
												</div> <!--.item-->
											</div>

											<?php
											// increment counter
											$item_counter++;
											}
										?>
									</div>

									<?php
								}				
							?>	

						</div>
					</div>

				</div> <!-- .container -->
			</div>

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();
