<?php
/**
 * Template Name: The Labs
 * 
 * The template for displaying the lab pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

// intro text
$intro_text_bg_color = get_field('field_61a0e08065bb6');
$intro_text_title = get_field('field_619b89c8cbb99');

// featured product
$featured_product_id = get_field('field_619cede818980');

$intro_heading = get_field('field_61965e77d5e53', $featured_product_id);
$intro_excerpt = get_field('field_61965e86d5e55', $featured_product_id);
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">
      
      <div class="intro-text"
        style="background: <?php echo $intro_text_bg_color; ?>">
        <div class="container">
          
          <div class="row">
            <div class="offset-lg-2 col-lg-8">

              <div class="text-center">
                <header class="mb-4">
                  <h1 class="font-bold text-2xl text-blue">
                    <?php echo $intro_text_title ? $intro_text_title : the_title(); ?>
                  </h1>
                </header>            
                <article class="font-light">
                  <?php the_content(); ?>
                </article>
              </div>

            </div>
          </div>

        </div> <!-- .container -->
      </div> <!--.intro-text-->

      <div class="anchors">
        <div class="container">
          <div class="row">

            <?php
              // Check rows exists.
              if( have_rows('field_619b9884328b1') ) {

                $menu_item_counter = 1;

                // Loop through rows.
                while( have_rows('field_619b9884328b1') ) { the_row();

                  // anchor or top menu
                  $top_menu = get_sub_field('field_619b988f328b2');
                  ?>
                    <div class="col-lg-4">
                      <a href="<?php echo esc_url( $top_menu['link']['url'] ); ?>"
                        class="text-black">
                        
                        <div class="<?php echo $menu_item_counter < count(get_field('field_619b9884328b1')) ? 'mb-32 lg:mb-0 ' : ''; ?> bg-white item">
                          <div class="text-center">
                            <img class="mb-4" src="<?php echo esc_url( $top_menu['icon']['url'] ); ?>" alt="<?php echo esc_attr( $top_menu['icon']['alt'] ); ?>">

                            <h3 class="mb-5 font-bold text-xl">
                              <?php echo $top_menu['title']; ?>
                            </h3>
                          </div>
                          <div class="font-light text-sm text-justify excerpt">
                            <?php echo wpautop( $top_menu['excerpt'] ); ?>
                          </div>
                        </div>
                      </a>
                    </div>
                  <?php

                  // set our counter
                  $menu_item_counter++;
                }
                
              }
            ?>

          </div>
        </div>
      </div>

      <?php
        // get_template_part('/template-parts/product-services/content', 'featured-product');
      ?>
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();