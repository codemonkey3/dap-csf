<?php
/**
 * Template Name: Contact Us
 * 
 * The template for displaying contact us pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

// get ACF value
$featured_image = get_field('field_619508f409536');
$shortcode_form = get_field('field_6195092209537');
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

      <div class="pb-17 intro-text">
        <div class="container">
          
          <div class="row">
            <div class="offset-lg-2 col-lg-8">
              
              <div class="text-center">
                <?php while ( have_posts() ) : the_post(); ?>
                  <header class="mb-4">
                    <h1 class="font-bold text-xl lg:text-2xl text-blue"><?php the_title(); ?></h1>
                  </header>

                  <article class="font-light">
                    <?php the_content(); ?>
                  </article>
                <?php endwhile; // End of the loop. ?>
              </div>

            </div>
          </div>

        </div> <!-- .container -->
      </div> <!-- .intro-text -->
      
      <div class="pb-12">
        <div class="container">
          <div class="row">
            <div class="col-lg-7">
              <figure class="mb-0">
                <img class="w-full lg:pr-4" src="<?php echo esc_url( $featured_image['url'] ); ?>" alt="<?php echo esc_attr( $featured_image['alt'] ); ?>">
              </figure>
            </div>
            <div class="col-lg-5">
              <div class="mt-6 lg:mt-0">
                <?php echo do_shortcode( $shortcode_form ); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();