<?php
/**
 * Template Name: About
 * 
 * The template for displaying about page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package james-walker
 */

get_header();

// get ACF value

// media & content
$media_content_featured_image = get_field('field_61925a662f6fd');
$media_content_heading = get_field('field_61925a772f6fe');
$media_content_excerpt = get_field('field_61925a872f6ff');
$media_content_fullcontent = get_field('field_61925ad62a05d');

// center for strategic futures
$strategic_futures_bg_color = get_field('field_6195ec46fb73f');
$strategic_futures_bg_image = get_field('field_61925a502f6fc');
$strategic_futures_heading = get_field('field_61925a2e2f6fa');
$strategic_futures_excerpt = get_field('field_61925a412f6fb');

// vission and mission
$vission_mission_bg_color = get_field('field_6195ecb2f7dfe');
$vission_mission_bg_image = get_field('field_6195ecbef7dff');
$vission_mission_heading = get_field('field_6195e8bc31017');
$vission_mission_excerpt = get_field('field_61925af12a05f');

// Mandates of the Academy
$mandate_bg_color = get_field('field_6195ed78ed7cc');
$mandate_heading = get_field('field_61925b23e8ade');
$mandate_excerpt = get_field('field_61925b38e8adf');

// Services
$services_bg_color = get_field('field_6229765040485');
$services_heading = get_field('field_622976c776e0d');
$services_items = get_field('field_62297510e39bb');

// the team
$team_bg_color = get_field('field_6195edeb7dd97');
$team_heading = get_field('field_61925b7ffc72f');
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">
			
			<div class="intro-text">
				<div class="container">
					
					<div class="row align-items-center">
						<div class="col-lg-7">
							<figure class="mt-12 lg:mt-0">
								<img class="w-full" src="<?php echo $media_content_featured_image['url']; ?>" alt="<?php echo $media_content_featured_image['alt']; ?>">
							</figure>
						</div>
						<div class="col-lg-5">
							<h1 class="mb-6 font-bold text-3xl lg:text-4xl text-blue">
								<?php echo $media_content_heading; ?>
							</h1>
							<div class="excerpt">
								<?php echo wpautop( $media_content_excerpt ); ?>
							</div>
						</div>
					</div>

				</div> <!-- .container -->
			</div>

			<div class="full-content">
				<div class="container">
					
					<div class="row">
						<div class="col-lg-12">
							<article class="font-light">
								<?php echo wpautop( $media_content_fullcontent ); ?>
							</article>
						</div>
					</div>
					
				</div>
			</div>

			<style>
				@media (min-width: 1024px) {
					.strategic-future .inner {
						background-image: url(<?php echo $strategic_futures_bg_image['url'] ?>);
						background-repeat: no-repeat;
						background-size: contain;
					}
				}
			</style>
			<div class="strategic-future">
				<div class="container">
					
					<div class="inner" style="background-color: <?php echo $strategic_futures_bg_color; ?>;">
						
						<div class="row">
							<div class="offset-lg-5 col-lg-7">
								<div class="px-8 py-17 text-white">
									<h2 class="mb-6 font-bold text-2xl">
										<?php echo $strategic_futures_heading; ?>
									</h2>
									<article class="font-light">
										<?php echo wpautop( $strategic_futures_excerpt ); ?>
									</article>
								</div>								
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="vission-mission"
				style="background-color: <?php echo $vission_mission_bg_color; ?>;
					background-image: url(<?php echo $vission_mission_bg_image['url'] ?>);
					background-repeat: no-repeat;
					background-position: center;">
				<div class="container">
					
					<div class="row justify-content-center">
						<div class="col-lg-10">

							<div class="copy text-center">
								<h2 class="mb-4 text-xl">
									<?php echo $vission_mission_heading; ?>
								</h2>
								<article class="font-light">
									<?php echo wpautop( $vission_mission_excerpt ); ?>
								</article>
							</div>

						</div>
					</div>
				
				</div>
			</div>

			<div class="py-12 mandate text-white"
			style="background: <?php echo $mandate_bg_color; ?>">
				<div class="container">
					<div class="row">
						<div class="offset-lg-2 col-lg-8">

							<div class="mb-10">
								<h2 class="mb-5 font-bold text-2xl text-center">
									<?php echo $mandate_heading; ?>
								</h2>
								<article class="font-light text-left">
									<?php echo wpautop( $mandate_excerpt ); ?>
								</article>
							</div>

							<div class="items">
								<?php
									// Check rows exists.
									if( have_rows('field_61925b44e8ae0') ):
										$x = 1;

										// Loop through rows.
										while( have_rows('field_61925b44e8ae0') ) : the_row();

											// Load sub field value.
											$description = get_sub_field('field_61925b5ee8ae1');
											?>
												<div class="item mb-8">
													<div class="flex">
														<span class="flex align-items-center justify-content-center mr-6 font-bold text-3xl counter">
															<?php echo $x; ?>
														</span>
														<div>
															<?php echo wpautop( $description ); ?>
														</div>
													</div>
												</div>
											<?php

										$x++;

										// End loop.
										endwhile;

									// No value.
									else :
										// Do something...
									endif;
								?>
							</div>

							<div class="pt-8 services">
								
								<div class="container">
									<div class="row">
										<div class="col-lg-12">

											<div class="text-center">
												<h2 class="mb-5 font-bold text-2xl text-center">
													<?php echo $services_heading; ?>
												</h2>
											</div>
											
											<?php echo wpautop( $services_items ); ?>

										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
			
			<div class="pt-12 pb-6 team"
				style="background: <?php echo $team_bg_color; ?>">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">

							<div class="text-center">
								<h2 class="mb-3 font-bold text-3xl text-white">
									<?php echo $team_heading; ?>
								</h2>
							</div>

						</div>
					</div>
					
					<div class="mb-9 president-ceo">
						<div class="row justify-content-center">
							
							<?php
								// Check rows exists.
								if( have_rows('field_6195eb1531023') ):
									// Loop through rows.
									while( have_rows('field_6195eb1531023') ) : the_row();
										$gravatar = get_sub_field('field_6195eb1531024');
										$personal_info = get_sub_field('field_6195eb1531025');

										if( $gravatar && "" != $gravatar['url'] ) {
											$gravatar_url = $gravatar['url'];
										}
										else {
											$gravatar_url = get_stylesheet_directory_uri() . '/images/avatar-placeholder.png';
										}
										?>
											<div class="col-lg-2">
												<div class="mb-9 text-white text-center team-card">
													<a href="javascript: void(0);" class="text-white block"
														data-avatar="<?php echo esc_url($gravatar_url); ?>"
														data-name="<?php echo $personal_info['name']; ?>"
														data-role="<?php echo $personal_info['role']; ?>"
														data-bio="<?php echo esc_html(wpautop($personal_info['bio'])); ?>">
														<figure class="mb-0">
															<img src="<?php echo $gravatar_url; ?>" alt="<?php echo $gravatar['alt']; ?>" width="136">
														</figure>
														
														<h3 class="mb-2 font-bold text-base">
															<?php echo $personal_info['name']; ?>
														</h3>

														<?php if( $personal_info['role'] ) { ?>
															<p class="mb-0 text-sm role">
																<?php echo $personal_info['role']; ?>
															</p>
														<?php } ?>
													</a>
												</div>
											</div>
										<?php
									// End loop.
									endwhile;
									
								// No value.
								else :
									// Do something...
								endif;
							?>
						</div>
					</div>

					<div class="mb-9 supervising-fellow">
						<div class="row justify-content-center">
							
							<?php
								// Check rows exists.
								if( have_rows('field_6195eaf93101d') ):
									// Loop through rows.
									while( have_rows('field_6195eaf93101d') ) : the_row();
										$gravatar = get_sub_field('field_6195eaf93101e');
										$personal_info = get_sub_field('field_6195eaf93101f');

										if( $gravatar && "" != $gravatar['url'] ) {
											$gravatar_url = $gravatar['url'];
										}
										else {
											$gravatar_url = get_stylesheet_directory_uri() . '/images/avatar-placeholder.png';
										}
										?>
											<div class="col-lg-2">
												<div class="mb-9 text-white text-center team-card">
													<a href="javascript: void(0);" class="text-white block"
														data-avatar="<?php echo esc_url($gravatar_url); ?>"
														data-name="<?php echo $personal_info['name']; ?>"
														data-role="<?php echo $personal_info['role']; ?>"
														data-bio="<?php echo wpautop($personal_info['bio']); ?>">
														<figure class="mb-0">
															<img src="<?php echo $gravatar_url; ?>" alt="<?php echo $gravatar['alt']; ?>" width="136">
														</figure>
														
														<h3 class="mb-2 font-bold text-base">
															<?php echo $personal_info['name']; ?>
														</h3>
														<p class="mb-0 text-sm role">
															<?php echo $personal_info['role']; ?>
														</p>
													</a>
												</div>
											</div>
										<?php
									// End loop.
									endwhile;
									
								// No value.
								else :
									// Do something...
								endif;
							?>
							
						</div>
					</div>

					<div class="others">
						<div class="row justify-content-center">
							
							<?php
								// Check rows exists.
								if( have_rows('field_6195e98231019') ) :

									$other_counter = 1;
								
									// Loop through rows.
									while( have_rows('field_6195e98231019') ) : the_row();
										$gravatar = get_sub_field('field_61925c187101b');
										$personal_info = get_sub_field('field_6195ea653101b');

										if( $gravatar && "" != $gravatar['url'] ) {
											$gravatar_url = $gravatar['url'];
										}
										else {
											$gravatar_url = get_stylesheet_directory_uri() . '/images/avatar-placeholder.png';
										}
										?>
											<div class="col-sm-6 col-md-3 col-lg-2">
												<div class="mb-9 text-white text-center team-card">
													<a href="javascript: void(0);" class="text-white block"
														data-avatar="<?php echo esc_url($gravatar_url); ?>"
														data-name="<?php echo $personal_info['name']; ?>"
														data-role="<?php echo $personal_info['role']; ?>"
														data-bio="<?php echo wpautop($personal_info['bio']); ?>">
														<figure class="mb-0">
															<img src="<?php echo $gravatar_url; ?>" alt="<?php echo $gravatar['alt']; ?>" width="136">
														</figure>
														
														<h3 class="mb-2 font-bold text-base">
															<?php echo $personal_info['name']; ?>
														</h3>
														<p class="mb-0 text-sm role">
															<?php echo $personal_info['role']; ?>
														</p>
													</a>
												</div>
											</div>
										<?php

										if( $other_counter == 6 ) {
											echo '</div><div class="row justify-content-center">';
										}

										// set our counter
										$other_counter++;
										
									// End loop.
									endwhile;
									
								// No value.
								else :
									// Do something...
								endif;
							?>
							
						</div>
					</div>
					
				</div>
			</div>

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();