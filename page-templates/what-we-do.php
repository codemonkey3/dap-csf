<?php
/**
 * Template Name: What We Do
 * 
 * The template for displaying what we do pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

// get ACF value
$intro_text_bg_color = get_field('field_61a0c5097da0f');
$intro_text_heading = get_field('field_61a0c519de7f4');
?>
	
	<main id="main" class="site-main m-0" role="main">
		<div id="primary" class="content-area">

      <div class="intro-text"
        style="background: <?php echo $intro_text_bg_color; ?>">
        <div class="container">
          
          <div class="row">
            <div class="offset-lg-1 col-lg-10">
              <header class="text-center">
                <h1 class="font-bold text-2xl text-blue">
                  <?php echo $intro_text_heading ? $intro_text_heading : the_title(); ?>
                </h1>
              </header>
            </div>
          </div> 

        </div> <!-- .container -->
      </div>

      <div class="intro-menus">
        <div class="container">
          <div class="row">
            <div class="offset-lg-1 col-lg-10">

              <div class="row">
                <?php
                  // Check rows exists.
                  if( have_rows('field_619a4a4c59d16') ):

                    // Loop through rows.
                    while( have_rows('field_619a4a4c59d16') ) : the_row();

                      // Load sub field value.
                      $content = get_sub_field('field_619a52ead1a62');
                      ?>
                      <div class="col-sm-6 col-lg-3">
                        <div class="mb-8 bg-white item">
                          <a href="#<?php echo sanitize_title( $content['heading'] ); ?>">
                            <div class="text-center">
                              <img class="mb-1" src="<?php echo esc_url( $content['icon']['url'] ); ?>" alt="<?php echo $content['heading'] ?>">
                              <h3 class="mb-0 font-bold text-lg text-blue"><?php echo $content['heading']; ?></h3>
                            </div>
                          </a>
                        </div>
                      </div>
                      <?php

                    // End loop.
                    endwhile;

                  endif;
                ?>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="sections">
        
        <?php
          // Check rows exists.
          if( have_rows('field_619a4a4c59d16') ) {
            
            $item_counter = 1;

            // Loop through rows.
            while( have_rows('field_619a4a4c59d16') ) { the_row();
              $media_alignment = get_sub_field('field_619a4b8059d1b');
              $featured_image = get_sub_field('field_619a4b7659d1a');
              $content = get_sub_field('field_619a52ead1a62');
              ?>

              <style>
                @media (min-width: 1024px) {
                  .item-<?php echo $item_counter; ?>:before {
                    position: absolute;                      
                    background-image: url(<?php echo esc_url($featured_image['url']); ?>);
                    background-repeat: no-repeat;
                    background-size: cover;
                    background-position: center;
                    width: 50%;
                    height: 100%;
                    content: "";
                  }

                  .item-<?php echo $item_counter; ?>.item-default:before {
                    left: 0;
                  }
                  .item-<?php echo $item_counter; ?>.item-right:before {
                    right: 0;
                  }
                }
              </style>
              <div id="<?php echo sanitize_title( $content['heading'] ); ?>" 
                class="relative item item-<?php echo $item_counter; ?> item-<?php echo 'right' == $media_alignment ? 'right' : 'default'; ?>"
                style="background-color: <?php echo $content['content_bg_color']; ?>; color: <?php echo $content['content_text_color'] ? $content['content_text_color'] : '#000'; ?>">
                <div class="container">
                  <div class="row align-items-center <?php echo 'right' == $media_alignment ? 'flex-row-reverse' : ''; ?>">
                    <div class="<?php echo 'right' == $media_alignment ? 'offset-lg-1' : ''; ?> col-lg-6">
                      <figure class="mt-12 mb-0 lg:mt-0 block lg:hidden">
                        <img src="<?php echo esc_url( $featured_image['url'] ); ?>" alt="<?php echo esc_attr( $featured_image['alt'] ); ?>">
                      </figure>
                    </div>
                    <div class="<?php echo 'left' == $media_alignment ? 'offset-lg-1' : ''; ?> col-lg-5">                      
                      <article class="font-light">
                        <div class="media align-items-center">
                          <img class="mb-2 icon" src="<?php echo esc_url( $content['icon']['url'] ); ?>" alt="<?php echo $content['heading']; ?>">
                          <h3 class="mb-0 font-bold text-xl lg:text-2xl text-uppercase">
                            <?php echo $content['heading']; ?>
                          </h3>
                        </div>
                        
                        <?php echo wpautop( $content['excerpt'] ); ?>
                      </article>
                    </div>
                  </div>
                </div> <!--container-->
              </div> 
              <?php

              // increment counter
              $item_counter++;
            }

          }
        ?>
                
      </div>
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();