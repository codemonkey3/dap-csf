<?php
/**
 * Template Name: Search
 * 
 * The template for displaying search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

$q = isset( $_GET['q'] ) ? $_GET['q'] : '';
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="mb-12 intro-text">
				<div class="container">
					
					<div class="row justify-content-center">
						<div class="col-lg-12">

							<div class="text-center">
								<header class="mb-7">
									<h1 class="mb-0 font-bold text-xl lg:text-2xl text-blue">
										<?php the_title(); ?>
									</h1>
								</header>

								<form role="search" method="GET" id="search_frm">
									<p class="mb-0">
										<label class="screen-reader-text" for="s">Search</label>
										<input type="search" name="q" id="q" class="has-icon align-right-icon search-icon" value="<?php echo $q; ?>" placeholder="Search" />
									</p>
								</form>
							</div>

						</div>
					</div>

				</div>
			</div>
				
			<div class="mb-12">
				<div class="container">
					
					<div class="row justify-content-center">
						<div class="col-lg-12">

							<div class="search-results">
								<?php
									$frontpage_id = get_option( 'page_on_front' );
									$blog_id = get_option( 'page_for_posts' );
									
									$args = [
										'post_type' => ['post', 'page', 'project'],
										'post__not_in' => [$frontpage_id, $blog_id, 66, get_the_ID()], // 58-whatwedo, 54-aboutus
										'_s' => $q,
										'meta_query' => [
											'relation' => 'OR',
											// about
											[
												'key'     => 'media_content_heading',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'media_content_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'full_content',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'strategic_futures_heading',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'strategic_futures_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'vision_mission_heading',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'vision_mission_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'mandates_academy_heading',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'mandates_academy_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'mandates_academy_items_$_description',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'services_heading',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'services_items',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'team_heading',
												'value'   => $q,
												'compare' => 'LIKE',
											],
											[
												'key'     => 'president_items_$_name',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'president_items_$_bio',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'supervising_fellow_items_$_name',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'supervising_fellow_items_$_bio',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'other_items_$_name',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'other_items_$_bio',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],

											// the labs
											[
												'key'     => 'intro_text_title',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'items_$_title',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'items_$_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'items_$_section_heading',
												'compare' => 'LIKE',
												'value'   => $q,
										 	],
											[
												'key'     => 'items_$_content',
												'compare' => 'LIKE',
												'value'   => $q,
										 	],
											
											// what we do
										 	[
												'key'     => 'intro_text_heading',
												'compare' => 'LIKE',
												'value'   => $q,
										 	],
											[
												'key'     => 'sections_$_heading',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'sections_$_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'sections_$_content',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											
											// projects
											[
												'key'     => 'hero_heading',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'hero_excerpt',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],

											// resources
											[
												'key'     => 'file_name',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											[
												'key'     => 'file_source',
												'value'   => $q,
												'compare' => 'LIKE',
										 	],
											 
										],
										'posts_per_page' => -1,
									];
									
									// The Query
									$the_query = new WP_Query( $args );
										
									// The Loop
									if ( $the_query->have_posts() ) {
										while ( $the_query->have_posts() ) { $the_query->the_post();
											?>
												<div class="mb-6 item">
													<h3 class="font-bold text-base text-blue">
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</h3>
													<article>
														<?php echo get_the_excerpt(); ?>
													</article>
												</div>
											<?php
										}
									}
									else {
										?>
										<p>😔 Sorry, no post found with your search term "<strong><?php echo $q; ?></strong>".</p>
										<?php
									}

									/* Restore original Post Data */
									wp_reset_postdata();
								?>
							</div>
								
						</div>
					</div>
				
				</div> <!-- .container -->
			</div>
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();