<?php
/**
 * Template Name: Resources
 * 
 * The template for displaying resource pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

$cta = get_field('field_6257964d42fd5');
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="mb-21 intro-text">
				<div class="container">

					<?php while ( have_posts() ) : the_post(); ?>
						<div class="row">
							<div class="offset-lg-2 col-lg-8">

								<div class="text-center">
									<header class="mb-4">
										<h1 class="font-bold text-xl lg:text-2xl text-blue"><?php the_title(); ?></h1>
									</header>
									
									<article class="font-light">
										<?php the_content(); ?>
									</article>
								</div>

							</div>
						</div>
					<?php endwhile; // End of the loop. ?>

				</div> <!--container-->
			</div>

			<div class="pb-12 lg:pb-17 resources">
				<div class="container">

					<?php
						// Check rows exists.
						if( have_rows('field_6194f8fdbc5f2') ) {

							// Loop through rows.
							while( have_rows('field_6194f8fdbc5f2') ) { the_row();

								// Load sub field value.
								$file_name = get_sub_field('field_6194f957bc5f3');
								$file_source = get_sub_field('field_6194f95fbc5f4');
								$user_access = get_sub_field('field_6232e1e8f8e82');

								if( 'public' == $user_access ) {
								?>
									<div class="mb-4 bg-gray-15 item">
										<div class="row align-items-center">
											<div class="offset-lg-1 col-lg-2">
												<div class="text-center lg:text-left">
													<?php if( 'pdf' == strtolower($file_source['subtype']) ) { ?>
														<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pdf-icon.png" alt="<?php echo $file_name ? $file_name : $file_source['title']; ?>">
													<?php } else if( 'jpg' == strtolower($file_source['subtype']) || 'jpeg' == strtolower($file_source['subtype']) ) { ?>
														<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/jpg-icon.png" alt="<?php echo $file_name ? $file_name : $file_source['title']; ?>">
													<?php } else if( 'png' == strtolower($file_source['subtype']) ) { ?>
														<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/png-icon.png" alt="<?php echo $file_name ? $file_name : $file_source['title']; ?>">
													<?php } else { ?>
														<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pdf-icon.png" alt="<?php echo $file_name ? $file_name : $file_source['title']; ?>">
													<?php } ?>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="text-center lg:text-left">
													<h3 class="mt-6 mb-2 lg:mt-0 font-bold text-lg lg:text-xl">
														<?php echo $file_name ? $file_name : $file_source['title']; ?>
													</h3>
													<p class="text-sm">
														<strong>Date Uploaded:</strong> <?php echo date('M. d Y', strtotime($file_source['date'])); ?> <br/>
														File size: <?php echo round($file_source['filesize'] / 1000); ?>MB 
														<span>|</span> 
														File format: <span class="text-uppercase">
														<?php echo $file_source['subtype']; ?></span>
													</p>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="text-center">
													<?php if( 'public' == $user_access ) { ?>
														<a class="btn" href="<?php echo $file_source['url'] ?>" download>
															Download the Guidebook
														</a>
													<?php }?>
												</div>
											</div>
										</div>
									</div>
								<?php
								}
							}

						}
					?>

				</div> <!-- .container -->
			</div>

			<?php if( $cta && "" !=  $cta['url'] ) { ?>
				<div class="pb-12 lg:pb-17 resources">
					<div class="container">
						<div class="text-center">
							<p>To access the private file you can request access
								<a class="btn btn-sm inline"
									href="mailto:<?php echo $cta['url']; ?>"
									target="_blank"
								>
									<?php echo $cta['title'] ?? 'Request to Access' ?>
								</a>
							</p>
						</div>
					</div>
				</div>
			<?php } ?>
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();