<?php
/**
 * Template Name: Careers
 * 
 * The template for displaying careers pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

// get ACF value
$hero_bg = get_field('field_6195052f68743');
$featured_image = get_field('field_6195053968744');
$copy = get_field('field_6195054668745');
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

      <div class="intro-text">
        <div class="container">
          
          <div class="row">
            <div class="offset-lg-2 col-lg-8">
              
              <div class="text-center">
                <?php while ( have_posts() ) : the_post(); ?>
                  <header class="mb-4">
                    <h1 class="font-bold text-xl lg:text-2xl text-blue"><?php the_title(); ?></h1>
                  </header>

                  <article class="font-light">
                    <?php the_content(); ?>
                  </article>
                <?php endwhile; ?>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="join-our-team"
        style="background-image: url(<?php echo esc_url($hero_bg['url']); ?>);
          background-repeat: no-repeat;
          background-position: center;
          background-size: cover;">
        <div class="container">

          <div class="row align-items-center">
            <div class="col-lg-7">
              <figure class="mb-0">
                <img src="<?php echo esc_url($featured_image['url']); ?>" alt="<?php echo esc_attr($featured_image['alt']); ?>">
              </figure>
            </div>
            <div class="col-lg-5">
              <h2 class="mt-6 mb-6 lg:mt-0 font-bold text-lg lg:text-xl text-blue text-uppercase">
                <?php echo $copy['title'] ?>
              </h2>
              <article class="font-light">
                <?php echo wpautop( $copy['excerpt'] ); ?>
              </article>

              <p>
                <a class="btn" href="<?php echo esc_url( $copy['cta']['url'] ); ?>">
                  <?php echo esc_attr( $copy['cta']['title'] ); ?>
                </a>
              </p>
            </div>
          </div>

        </div> <!-- .container -->
      </div> <!-- .join-our-team -->	
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();