<?php
/**
 * Template Name: The Lab Single
 * 
 * The template for displaying the lab pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">
      
      <?php
        // Check rows exists.
        if( have_rows('field_6228b78a9cf89') ) {
          
          // Loop through rows.
          while( have_rows('field_6228b78a9cf89') ) { the_row();
            ?>
              <section>

                <div class="container">

                  <div class="py-12">
                    <div class="row">
                      <div class="col-lg-12">
                        <h2 class="mb-10 lg:mb-13 font-bold text-blue text-2xl lg:text-3xl text-center">
                          <?php the_title() ?>
                        </h2>
                      </div>
                    </div>
                  </div>

                  <?php
                    // Check rows exists.
                    if( have_rows('field_6228b78ae3a58') ) {
                      
                      // Loop through rows.
                      while( have_rows('field_6228b78ae3a58') ) { the_row();
                        $media_alignment = get_sub_field('field_6228b78af3ef1');
                        $media = get_sub_field('field_6228b78b0378d');
                        $content = get_sub_field('field_6228b78b039b1');
                        $has_cta = get_sub_field('field_6228b78b04a6a');
                        $cta_ui = get_sub_field('field_6228b78b055b1');
                        $cta_icon = get_sub_field('field_6228b78b055ec');
                        $cta_link = get_sub_field('field_6228b78b0565c');
                        ?>
                        <div class="pb-12">
                          <div class="row align-items-center <?php echo 'right' == $media_alignment ? 'flex-row-reverse' : ''; ?>">
                            
                            <?php if( $media && "" != $media['url'] ) { ?>

                              <div class="col-lg-6">
                                <figure class="mb-6 lg:mb-0 text-center lg:text-left">
                                  <img src="<?php echo esc_url($media['url']); ?>" alt="<?php echo esc_attr($media['alt']); ?>">
                                </figure>
                              </div>
                              <div class="col-lg-6">
                                <article class="font-light <?php echo 'right' == $media_alignment ? 'lg:pr-8' : 'lg:pl-8'; ?> text-justify">
                                  <?php echo wpautop( $content ); ?>
                                  
                                  <?php if( $has_cta ) { ?>
                                    <a href="<?php echo $cta_link['url']; ?>" title="<?php echo esc_attr( $cta_link['title'] ); ?>"
                                      class="btn btn-<?php echo $cta_ui; ?> <?php echo 'ghost' == $cta_ui ? 'font-bold' : ''; ?>"
                                      style="color: <?php echo 'ghost' == $cta_ui ? '#cfb034' : ''; ?>">
                                      <div class="flex align-items-center justify-content-between">
                                        <?php if( $cta_icon ) { ?>
                                          <?php echo $cta_icon; ?>
                                        <?php } ?>
                                        <span class="<?php echo $cta_icon ? 'ml-2' : '' ?>"><?php echo esc_attr( $cta_link['title'] ); ?></span>
                                      </div>
                                    </a>
                                  <?php } ?>
                                </article>
                              </div>

                            <?php } else { ?>

                              <div class="col-lg-12">
                                <article class="font-light text-justify">
                                  <?php echo wpautop( $content ); ?>
                                </article>
                              </div>

                            <?php } ?>

                          </div>
                        </div>
                        <?php
                      }

                    }
                  ?>
                  
                </div>
              </section>
            <?php

          }
          
        }
      ?>

      <?php
        // get_template_part('/template-parts/product-services/content', 'featured-product');
      ?>
			
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();