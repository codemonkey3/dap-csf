/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function () {
  var container, button, menu, links, subMenus, i, len;

  container = document.getElementById("site-navigation");
  if (!container) {
    return;
  }

  button = container.getElementsByTagName("button")[0];
  if ("undefined" === typeof button) {
    return;
  }

  menu = container.getElementsByTagName("ul")[0];

  // Hide menu toggle button if menu is empty and return early.
  if ("undefined" === typeof menu) {
    button.style.display = "none";
    return;
  }

  menu.setAttribute("aria-expanded", "false");
  if (-1 === menu.className.indexOf("nav-menu")) {
    menu.className += " nav-menu";
  }

  button.onclick = function () {
    if (-1 !== container.className.indexOf("toggled")) {
      container.className = container.className.replace(" toggled", "");
      button.setAttribute("aria-expanded", "false");
      menu.setAttribute("aria-expanded", "false");
    } else {
      container.className += " toggled";
      button.setAttribute("aria-expanded", "true");
      menu.setAttribute("aria-expanded", "true");
    }
  };

  // Get all the link elements within the menu.
  links = menu.getElementsByTagName("a");
  subMenus = menu.getElementsByTagName("ul");

  // Set menu items with submenus to aria-haspopup="true".
  for (i = 0, len = subMenus.length; i < len; i++) {
    subMenus[i].parentNode.setAttribute("aria-haspopup", "true");
  }

  // Each time a menu link is focused or blurred, toggle focus.
  for (i = 0, len = links.length; i < len; i++) {
    links[i].addEventListener("focus", toggleFocus, true);
    links[i].addEventListener("blur", toggleFocus, true);
  }

  /**
   * Sets or removes .focus class on an element.
   */
  function toggleFocus() {
    var self = this;

    // Move up through the ancestors of the current link until we hit .nav-menu.
    while (-1 === self.className.indexOf("nav-menu")) {
      // On li elements toggle the class .focus.
      if ("li" === self.tagName.toLowerCase()) {
        if (-1 !== self.className.indexOf("focus")) {
          self.className = self.className.replace(" focus", "");
        } else {
          self.className += " focus";
        }
      }

      self = self.parentElement;
    }
  }

})();

/**
 * @desc    This contains all scripts use in site themes
 */
(function ($) {
  "use strict";

  if(window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
  }
  if(window.HTMLCollection && !HTMLCollection.prototype.forEach) {
    HTMLCollection.prototype.forEach = Array.prototype.forEach;
  }

  /**
   * All of the code for your public-facing JavaScript source
   * should reside in this file.
   *
   * Note: It has been assumed you will write jQuery code here, so the
   * $ function reference has been prepared for usage within the scope
   * of this function.
   *
   * This enables you to define handlers, for when the DOM is ready:
   *
   * $(function() {
   *
   * });
   *
   * When the window is loaded:
   *
   * $( window ).load(function() {
   *
   * });
   *
   * ...and/or other possibilities.
   *
   * Ideally, it is not considered best practise to attach more than a
   * single DOM-ready or window-load handler for a particular page.
   * Although scripts in the WordPress core, Plugins and Themes may be
   * practising this, we should strive to set a better example in our own work.
   */

  /* Process mobile menu */
  function dap_csf_deal_with_mobile_nav() {
    if ($("body").hasClass("slide-menu-active")) {
      $("body").removeClass("slide-menu-active");
    } else {
      // Set up our sub-menus
      var el = $('<span class="mobiMenuChild"></span>');

      if (el.parent().length === 0) {
        $("#mobile-menu li.menu-item-has-children").append(el);
      }
      
      // Hide the sub-menus
      $("#mobile-menu li.menu-item-has-children").toggleClass("hidey", true);

      $("body").addClass("slide-menu-active");

      $("html, body").animate({
          scrollTop: $(".site-header").offset().top,
        },
        200
      );
    }
  }

  $(".menu-toggle, .close-menu-btn").click(function (event) {
    event.preventDefault();
    dap_csf_deal_with_mobile_nav();
  });
  
  $(document).on("click", ".mobiMenuChild", function (event) {
    event.stopPropagation();
    event.preventDefault();

    if (event.handled !== true) {
      $(this).parent().toggleClass("hidey");

      event.handled = true;
    } else {
      return false;
    }
  });

  /**
   * Add global site changes HERE...
   */

  // Load below once the DOM is ready
  $(function() {
    /**
     * Home
     */
    let heroSlider = document.querySelector('.hero-slider');
    if( heroSlider ) {
      $('.hero-slider').slick({
        infinite: true,
        slidesToShow: 1,
      });
    }

    // Sticky Menu
    $(".header-top").stick_in_parent({
      bottoming: false
    });
    $(".main-navigation").stick_in_parent({
      offset_top: $('.header-top').outerHeight()
    });
    
  });

})(jQuery);

// Defer video / iframe and load only once the page is fully loaded
let init = () => {

  // defer video to save speed
  deferVideo();
  
  // post category filter
  postCategoryFilter();

  // about
  teamBio();

  // accessibility
  accessibilityMenu();
}

let deferVideo = () => {
  var vidDefer = document.getElementsByTagName('iframe');
  for (var i=0; i<vidDefer.length; i++) {
    if(vidDefer[i].getAttribute('data-src')) {
      vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
    } 
  }
}

/**
 * Widget
  */
let postCategoryFilter = () => {
  let postCategories = document.querySelectorAll('.post-categories > li > a');
  
  if( postCategories.length > 0 ) {
    postCategories.forEach(el => {
      el.addEventListener('click', e => {
        e.preventDefault();
        
        let childMenu = el.nextElementSibling;
        if( null != childMenu ) {
          if (childMenu.style.display === "none") {
            childMenu.style.display = 'block';
            el.classList.add('active');
          }
          else {
            childMenu.style.display = 'none';
            el.classList.remove('active');
          }
        }
      });
    });
  }
}

// team bio
let teamBio = () => {
  let teamCardItems = document.querySelectorAll('.team-card > a');
  if( teamCardItems.length > 0 ) {

    teamCardItems.forEach((el, i) => {
      el.addEventListener('click', e => {
        
        e.preventDefault();
        
        // remove all previously displayed bio
        removeActiveBio()
        removeActiveClass(i)
        
        let avatar = el.getAttribute('data-avatar'),
          name = el.getAttribute('data-name'),
          role = el.getAttribute('data-role'),
          bio = el.getAttribute('data-bio');

        let teamBioHTML = `
          <div class="block lg:hidden text-right lg:text-left">
            <a href="javascript: void(0);" class="close">close</a>
          </div>
          <div class="row align-items-center">
            <div class="col-lg-4">
              <figure class="mb-0 text-center lg:text-left">
                <img class="w-full" src="${avatar}" alt="${name}" />
              </figure>
            </div>
            <div class="col-lg-8">
              <div class="text-center lg:text-left">
                <h3 class="mb-2 font-bold text-3xl lg:text-4xl text-blue">${name}</h3>
                <p class="mb-4 text-lg role">${role}</p>
                
                <article class="font-light">
                  ${bio}
                </article>
              </div>
            </div>
          </div>`;
        
        // add popup data
        let parentRow = el.parentElement.parentElement.parentElement;
        let teamBioEl = document.createElement('div');

        teamBioEl.classList.add('relative', 'p-6', 'mb-6', 'bg-white', 'team-bio');
        teamBioEl.innerHTML = teamBioHTML;
        
        if( el.classList.contains('active') ) {
          el.classList.remove('active')
        }
        else {
          parentRow.insertAdjacentElement('afterend', teamBioEl);
          el.classList.add('active')
        }
        
      });
    });

    let removeActiveBio = () => {
      let teamBioItems = document.querySelectorAll('.team-bio');
      
      if( teamBioItems.length > 0 ) {
        teamBioItems.forEach(el => {
          el.remove();
        });
      }
    }

    let removeActiveClass = (currenItem) => {
      let teamCardItems = document.querySelectorAll('.team-card > a');
      
      teamCardItems.forEach((el, i) => {
        if( i != currenItem ) {
          el.classList.remove('active');
        }
      })
    }
  }
  
  let closeBioBtn = document.querySelector('.team');
  if( closeBioBtn ) {
    closeBioBtn.addEventListener('click', e => {

      // get current target
      const {target} = e;

      if( target.matches('a') ) {
        // remove old data
        let teamBioItems = document.querySelectorAll('.team-bio');
        if( teamBioItems.length > 0 ) {
          teamBioItems.forEach(el => {
            el.remove();
          });
        }

      }
    });
  }
}

let accessibilityMenu = () => {
  let accesibilityItem = document.querySelector('.accessibility');
  if( accesibilityItem ) {
    accesibilityItem.addEventListener('mouseenter', e => {
      let accesibilityMenuHTML = accesibilityItem.nextElementSibling;
      
      accesibilityMenuHTML.style.display = 'block';
    });
  }

  let accesibilityMenu = document.querySelector('.accessibility-menu');
  if( accesibilityMenu ) {
    accesibilityMenu.addEventListener('mouseleave', e => {
      accesibilityMenu.style.display = 'none';
    });
  }

  // font sizer
  let accessibilityFontsize = document.getElementById('accessibility-fontsize');

  if (getCookie('a11y-larger-fontsize')) {
    document.body.classList.add('fontsize');
    document.querySelector('head').innerHTML += `<link href="${theme_ajax.theme_url}/css/accessibility/a11y-fontsize.css" id="fontsizeStylesheet" rel="stylesheet" type="text/css" />`;
    accessibilityFontsize.setAttribute('aria-checked', true);
    accessibilityFontsize.classList.add('active');
  };

  accessibilityFontsize.addEventListener('click', e => {
    e.preventDefault();

    if( accessibilityFontsize.classList.contains('active') ) {
      document.getElementById('fontsizeStylesheet').remove();
      document.body.classList.remove('fontsize');
      accessibilityFontsize.removeAttribute('aria-checked');
      accessibilityFontsize.classList.remove('active');
      deleteCookie('a11y-larger-fontsize');
    }
    else {
      document.querySelector('head').innerHTML += `<link href="${theme_ajax.theme_url}/css/accessibility/a11y-fontsize.css" id="fontsizeStylesheet" rel="stylesheet" type="text/css" />`;
      document.body.classList.add('fontsize');
      accessibilityFontsize.setAttribute('aria-checked', true);
      accessibilityFontsize.classList.add('active');
      setCookie('a11y-larger-fontsize', '1');
    }
  });

  // high contrast
  let accessibilityContrast = document.getElementById('accessibility-contrast');

  if (getCookie('a11y-high-contrast')) {
    document.body.classList.add('contrast');
    document.querySelector('head').innerHTML += `<link href="${theme_ajax.theme_url}/css/accessibility/a11y-contrast.css" id="highContrastStylesheet" rel="stylesheet" type="text/css" />`;
    accessibilityContrast.setAttribute('aria-checked', true);
    accessibilityContrast.classList.add('active');
  }

  accessibilityContrast.addEventListener('click', e => {
    e.preventDefault();
    
    if( accessibilityContrast.classList.contains('active') ) {
      document.getElementById('highContrastStylesheet').remove();
      document.body.classList.remove('contrast');
      accessibilityContrast.removeAttribute('aria-checked');
      accessibilityContrast.classList.remove('active');
      deleteCookie('a11y-high-contrast');
    }
    else {
      document.querySelector('head').innerHTML += `<link href="${theme_ajax.theme_url}/css/accessibility/a11y-contrast.css" id="highContrastStylesheet" rel="stylesheet" type="text/css" />`;
      document.body.classList.add('contrast');
      accessibilityContrast.setAttribute('aria-checked', true);
      accessibilityContrast.classList.add('active');
      setCookie('a11y-high-contrast', '1');
    }
  });

  // skip content
  let accessibilitySkipContent = document.getElementById('accessibility-skip-content');
  accessibilitySkipContent.addEventListener('click', e => {
    let main = document.getElementById('main'),
      mainPosition = main.offsetTop;

    window.scrollTo({
      top: mainPosition - 117,
      left: 0,
      behavior: 'smooth'
    });
  });

  // skip to footer
  let accessibilitySkipFooter = document.getElementById('accessibility-skip-footer');
  accessibilitySkipFooter.addEventListener('click', e => {
    let footer = document.getElementById('colophon'),
      footerPosition = footer.offsetTop;

    window.scrollTo({
      top: footerPosition,
      left: 0,
      behavior: 'smooth'
    });
  });
}

let setCookie = (cname, cvalue, exdays) => {
  const d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  let expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

let getCookie = (cname) => {
  let name = cname + "=";
  let ca = document.cookie.split(';');

  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

let deleteCookie = (cname) => {
  document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

window.addEventListener('load', init());