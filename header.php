<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dap-csf
 */

$options = get_option( 'dap_csf_theme_options' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();

// display tracking or analytics code
if( isset( $options['header_analytics'] ) ) {
	echo $options['header_analytics'];
}

// get ACF value
$fb_username = get_field('field_619e3bfd14d64', 'option');
$twitter_username = get_field('field_619e3c2314d65', 'option');
$linkedin_username = get_field('field_619e3c3d14d66', 'option');
?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'dap-csf' ); ?></a>
	
	<?php
	$masthead_style = '';
	if( get_header_image() ) {
		$masthead_style = 'background-image: url('. get_header_image() .') no-repeat 0 0; background-size: cover';
	}
	?>
	<header id="masthead" class="site-header" role="banner" style="<?php echo $masthead_style; ?>">

		<div class="header-top">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-7">

						<div class="hidden lg:flex align-items-center">
							<svg width="43" height="10" viewBox="0 0 43 10" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M6.7207 3.05859C6.58398 2.46875 6.25 2.05664 5.71875 1.82227C5.42188 1.69336 5.0918 1.62891 4.72852 1.62891C4.0332 1.62891 3.46094 1.89258 3.01172 2.41992C2.56641 2.94336 2.34375 3.73242 2.34375 4.78711C2.34375 5.84961 2.58594 6.60156 3.07031 7.04297C3.55469 7.48438 4.10547 7.70508 4.72266 7.70508C5.32812 7.70508 5.82422 7.53125 6.21094 7.18359C6.59766 6.83203 6.83594 6.37305 6.92578 5.80664H4.92773V4.36523H8.52539V9H7.33008L7.14844 7.92188C6.80078 8.33203 6.48828 8.62109 6.21094 8.78906C5.73438 9.08203 5.14844 9.22852 4.45312 9.22852C3.30859 9.22852 2.37109 8.83203 1.64062 8.03906C0.878906 7.24219 0.498047 6.15234 0.498047 4.76953C0.498047 3.37109 0.882813 2.25 1.65234 1.40625C2.42188 0.5625 3.43945 0.140625 4.70508 0.140625C5.80273 0.140625 6.68359 0.419922 7.34766 0.978516C8.01562 1.5332 8.39844 2.22656 8.49609 3.05859H6.7207ZM14.0742 7.71094C14.7891 7.71094 15.3555 7.44727 15.7734 6.91992C16.1953 6.39258 16.4062 5.64258 16.4062 4.66992C16.4062 3.70117 16.1953 2.95312 15.7734 2.42578C15.3555 1.89453 14.7891 1.62891 14.0742 1.62891C13.3594 1.62891 12.7891 1.89258 12.3633 2.41992C11.9375 2.94727 11.7246 3.69727 11.7246 4.66992C11.7246 5.64258 11.9375 6.39258 12.3633 6.91992C12.7891 7.44727 13.3594 7.71094 14.0742 7.71094ZM18.2109 4.66992C18.2109 6.2207 17.7754 7.4082 16.9043 8.23242C16.252 8.9043 15.3086 9.24023 14.0742 9.24023C12.8398 9.24023 11.8965 8.9043 11.2441 8.23242C10.3691 7.4082 9.93164 6.2207 9.93164 4.66992C9.93164 3.08789 10.3691 1.90039 11.2441 1.10742C11.8965 0.435547 12.8398 0.0996094 14.0742 0.0996094C15.3086 0.0996094 16.252 0.435547 16.9043 1.10742C17.7754 1.90039 18.2109 3.08789 18.2109 4.66992ZM24.6504 0.363281H26.5254L23.5781 9H21.873L18.9551 0.363281H20.8828L22.7578 6.91992L24.6504 0.363281ZM32.4609 3.11133C32.4609 2.66211 32.3418 2.3418 32.1035 2.15039C31.8691 1.95898 31.5391 1.86328 31.1133 1.86328H29.4316V4.40625H31.1133C31.5391 4.40625 31.8691 4.30273 32.1035 4.0957C32.3418 3.88867 32.4609 3.56055 32.4609 3.11133ZM34.248 3.09961C34.248 4.11914 33.9902 4.83984 33.4746 5.26172C32.959 5.68359 32.2227 5.89453 31.2656 5.89453H29.4316V9H27.6387V0.363281H31.4004C32.2676 0.363281 32.959 0.585938 33.4746 1.03125C33.9902 1.47656 34.248 2.16602 34.248 3.09961ZM35.5898 9V0.363281H37.377V3.65625H40.752V0.363281H42.5449V9H40.752V5.14453H37.377V9H35.5898Z" fill="black"/>
							</svg>
							
							<nav role="navigation">
								<?php wp_nav_menu( array( 
									'theme_location' => 'top_menu',
									'menu_id' 		 => 'top-menu',
									'menu_class' 	 => 'top-menu list-unstyled ml-0 mb-0 flex',
									'container' 	 => 'ul'
								) ); ?>
							</nav>
						</div>
						
					</div>
					<div class="col-lg-5">
						<div class="flex justify-content-between justify-content-lg-end">
							<div class="flex align-items-center justify-content-center justify-content-lg-end">
								<a class="hidden lg:block" href="<?php echo esc_url( get_the_permalink(66) ); ?>">Sitemap</a>
								<?php get_search_form(); ?>
								
								<div class="relative">
									<a class="h-1.5 block text-right accessibility" href="javascript: void(0);" style="height: 24px;">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
											<circle cx="12" cy="12" r="11.5" stroke="black"/>
											<circle cx="12" cy="12" r="10.1538" fill="black"/>
											<path d="M12 5.35388C11.5042 5.35388 11.0288 5.55082 10.6783 5.90137C10.3277 6.25192 10.1308 6.72736 10.1308 7.22311C10.1308 7.71886 10.3277 8.19431 10.6783 8.54486C11.0288 8.89541 11.5042 9.09234 12 9.09234C12.4957 9.09234 12.9712 8.89541 13.3217 8.54486C13.6723 8.19431 13.8692 7.71886 13.8692 7.22311C13.8692 6.72736 13.6723 6.25192 13.3217 5.90137C12.9712 5.55082 12.4957 5.35388 12 5.35388ZM7.71987 7.70247C7.42342 7.59054 7.09464 7.60096 6.80586 7.73144C6.51709 7.86192 6.29197 8.10178 6.18004 8.39824C6.06811 8.69469 6.07854 9.02347 6.20902 9.31225C6.3395 9.60102 6.57935 9.82614 6.87581 9.93807L9.50769 10.9516V12.9455L8.08209 16.875C7.97581 17.1898 7.99658 17.5336 8.13997 17.8333C8.28336 18.133 8.53806 18.3649 8.84985 18.4797C9.16164 18.5944 9.5059 18.583 9.80937 18.4477C10.1128 18.3125 10.3516 18.0642 10.4747 17.7556L11.7308 14.3993C11.752 14.3445 11.7892 14.2975 11.8376 14.2642C11.886 14.231 11.9434 14.2133 12.0021 14.2133C12.0608 14.2133 12.1181 14.231 12.1665 14.2642C12.2149 14.2975 12.2522 14.3445 12.2733 14.3993L13.5311 17.7348C13.6516 18.0457 13.8895 18.2967 14.1935 18.4337C14.4975 18.5707 14.8431 18.5827 15.1558 18.4672C15.4686 18.3516 15.7233 18.1177 15.8651 17.8159C16.0069 17.5141 16.0244 17.1687 15.9138 16.8542L14.4923 12.9363V10.9491L17.1042 9.958C17.4006 9.84762 17.641 9.62403 17.7724 9.33642C17.9039 9.04882 17.9158 8.72075 17.8054 8.4244C17.695 8.12806 17.4714 7.8877 17.1838 7.7562C16.8962 7.62471 16.5682 7.61285 16.2718 7.72324L14.6535 8.31557C14.5572 8.35074 14.4773 8.42028 14.4292 8.5108C14.1948 8.95351 13.844 9.32377 13.4146 9.5816C12.9851 9.83942 12.4934 9.97502 11.9925 9.97374C11.4916 9.97246 11.0006 9.83434 10.5725 9.57432C10.1444 9.31429 9.79542 8.94224 9.56335 8.49834C9.51556 8.40719 9.43562 8.33703 9.33904 8.30145L7.71987 7.7033V7.70247Z" fill="white"/>
										</svg>
									</a>
									
									<ul class="absolute pt-2 pb-2 mb-0 ml-0 pl-0 list-none accessibility-menu" style="display: none;">
										<li>
											<a href="javascript: void(0);" id="accessibility-fontsize" class="block toggle-fontsize" title="Font Sizer">
												<span class="show-for-sr">Font Sizer</span>
												
												<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="font" class="svg-inline--fa fa-font fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
													<path fill="currentColor" d="M432 416h-23.41L277.88 53.69A32 32 0 0 0 247.58 32h-47.16a32 32 0 0 0-30.3 21.69L39.41 416H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h128a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16h-19.58l23.3-64h152.56l23.3 64H304a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h128a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zM176.85 272L224 142.51 271.15 272z"></path>
												</svg>
											</a>
										</li>
										<?php /* <li>
											<a href="javascript: void(0);" id="accessibility-contrast" class="block toggle-contrast" title="Toggle High Contrast">
												<span class="show-for-sr">High Contrast</span>
												
												<svg width="24" height="24" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="low-vision" class="svg-inline--fa fa-low-vision fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
													<path fill="currentColor" d="M569.344 231.631C512.96 135.949 407.81 72 288 72c-28.468 0-56.102 3.619-82.451 10.409L152.778 10.24c-7.601-10.858-22.564-13.5-33.423-5.9l-13.114 9.178c-10.86 7.601-13.502 22.566-5.9 33.426l43.131 58.395C89.449 131.73 40.228 174.683 6.682 231.581c-.01.017-.023.033-.034.05-8.765 14.875-8.964 33.528 0 48.739 38.5 65.332 99.742 115.862 172.859 141.349L55.316 244.302A272.194 272.194 0 0 1 83.61 208.39l119.4 170.58h.01l40.63 58.04a330.055 330.055 0 0 0 78.94 1.17l-189.98-271.4a277.628 277.628 0 0 1 38.777-21.563l251.836 356.544c7.601 10.858 22.564 13.499 33.423 5.9l13.114-9.178c10.86-7.601 13.502-22.567 5.9-33.426l-43.12-58.377-.007-.009c57.161-27.978 104.835-72.04 136.81-126.301a47.938 47.938 0 0 0 .001-48.739zM390.026 345.94l-19.066-27.23c24.682-32.567 27.711-76.353 8.8-111.68v.03c0 23.65-19.17 42.82-42.82 42.82-23.828 0-42.82-19.349-42.82-42.82 0-23.65 19.17-42.82 42.82-42.82h.03c-24.75-13.249-53.522-15.643-79.51-7.68l-19.068-27.237C253.758 123.306 270.488 120 288 120c75.162 0 136 60.826 136 136 0 34.504-12.833 65.975-33.974 89.94z"></path>
												</svg>
											</a>
										</li> */ ?>
										<li>
											<a href="javascript: void(0);" id="accessibility-skip-content" class="block" title="Skip to Content">
												<span class="show-for-sr">Skip to Content</span>
												
												<svg width="24" height="24" aria-hidden="true" focusable="false" data-prefix="far" data-icon="arrow-alt-circle-down" class="svg-inline--fa fa-arrow-alt-circle-down fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
													<path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm-32-316v116h-67c-10.7 0-16 12.9-8.5 20.5l99 99c4.7 4.7 12.3 4.7 17 0l99-99c7.6-7.6 2.2-20.5-8.5-20.5h-67V140c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12z"></path>
												</svg>
											</a>
										</li>
										<li>
											<a href="javascript: void(0);" id="accessibility-skip-footer" class="block" title="Skip to Footer">
												<span class="show-for-sr">Skip to Footer</span>
												
												<svg width="24" height="24" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" class="svg-inline--fa fa-chevron-down fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
													<path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
												</svg>
											</a>
										</li>
									</ul>
								</div>
							</div>

							<button class="block lg:hidden ml-4 menu-toggle" aria-controls="primary-menu" aria-expanded="false">
								<span></span>
								<span></span>
								<span></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="header-bottom">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6">

						<div class="site-branding">
							
							<div class="text-center lg:text-left">
								<?php
									$custom_logo_id = get_theme_mod( 'custom_logo' );
									$logo 			= wp_get_attachment_image_src( $custom_logo_id , 'full' );
									
									if ( is_front_page() && is_home() ) { ?>
										
										<h1 class="site-title mb-0">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
												<?php if( has_custom_logo() ) { ?>

													<img class="d-block" src="<?php echo esc_url( $logo[0] ); ?>" alt="<?php get_bloginfo('name') ?>" />

												<?php } else { ?>

													<?php bloginfo( 'name' ); ?>
													
												<?php } ?>
											</a>
										</h1>
										
									<?php } else { ?>

										<p class="site-title mb-0">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
												<?php if( has_custom_logo() ) { ?>

													<img src="<?php echo esc_url( $logo[0] ); ?>" alt="<?php bloginfo('name') ?>" />

												<?php } else { ?>

													<img class="d-block" src="<?php echo esc_url( $logo[0] ); ?>" alt="<?php get_bloginfo('name') ?>" />

												<?php } ?>
											</a>
										</p>

									<?php } ?>
							</div>

							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<p class="site-description hidden"><?php echo $description; /* WPCS: xss ok. */ ?></p>
							<?php
							endif; ?>
						</div><!-- .site-branding -->
						
					</div>
					
					<div class="col-lg-6">
						
						<div class="mt-9 lg:mt-0 text-center lg:text-right">
							<p class="font-light text-xs">Philippine Standard Time
								<time id="pst-time" class="block"></time>
							</p>

							<div class="flex justify-content-center justify-content-lg-end">
								<?php if( $fb_username ) { ?>
									<a href="https://www.facebook.com/<?php echo $fb_username; ?>" class="mr-2" target="_blank">
										<svg width="31" height="30" viewBox="0 0 31 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<g clip-path="url(#clip0_164_473)">
												<path d="M15.2934 0.600037C7.34043 0.600037 0.893433 7.04704 0.893433 15C0.893433 22.953 7.34043 29.4 15.2934 29.4C23.2464 29.4 29.6934 22.953 29.6934 15C29.6934 7.04704 23.2464 0.600037 15.2934 0.600037ZM18.7044 10.551H16.5399C16.2834 10.551 15.9984 10.8885 15.9984 11.337V12.9H18.7059L18.2964 15.129H15.9984V21.8205H13.4439V15.129H11.1264V12.9H13.4439V11.589C13.4439 9.70804 14.7489 8.17954 16.5399 8.17954H18.7044V10.551Z" fill="#1773EA"/>
											</g>
											<defs>
												<clipPath id="clip0_164_473">
													<rect width="30" height="30" fill="white" transform="translate(0.293457)"/>
												</clipPath>
											</defs>
										</svg>
									</a>
								<?php } ?>

								<?php if( $twitter_username ) { ?>
									<a href="https://twitter.com/<?php echo $twitter_username; ?>" class="mr-2" target="_blank">
										<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M15 0.600037C7.04698 0.600037 0.599976 7.04704 0.599976 15C0.599976 22.953 7.04698 29.4 15 29.4C22.953 29.4 29.4 22.953 29.4 15C29.4 7.04704 22.953 0.600037 15 0.600037ZM20.8575 12.396C20.8635 12.519 20.865 12.642 20.865 12.762C20.865 16.512 18.0135 20.8335 12.7965 20.8335C11.2549 20.8361 9.74543 20.3934 8.44948 19.5585C8.66998 19.5855 8.89648 19.596 9.12598 19.596C10.455 19.596 11.6775 19.1445 12.648 18.3825C12.0566 18.3709 11.4835 18.1751 11.0086 17.8223C10.5338 17.4695 10.1808 16.9774 9.99898 16.4145C10.4237 16.4953 10.8613 16.4784 11.2785 16.365C10.6365 16.2352 10.0593 15.8874 9.64454 15.3805C9.2298 14.8737 9.00314 14.239 9.00298 13.584V13.5495C9.38548 13.761 9.82348 13.89 10.2885 13.905C9.68667 13.5044 9.26065 12.889 9.09754 12.1847C8.93443 11.4804 9.04655 10.7404 9.41098 10.116C10.1234 10.992 11.0119 11.7087 12.0188 12.2195C13.0258 12.7303 14.1288 13.024 15.2565 13.0815C15.1131 12.473 15.1748 11.8341 15.432 11.2643C15.6892 10.6944 16.1274 10.2255 16.6786 9.93043C17.2298 9.63534 17.863 9.53062 18.4798 9.63255C19.0966 9.73447 19.6625 10.0373 20.0895 10.494C20.7242 10.3685 21.3329 10.1356 21.8895 9.80554C21.6779 10.4627 21.235 11.0208 20.643 11.376C21.2052 11.3083 21.7543 11.1572 22.272 10.9275C21.8917 11.4974 21.4127 11.9947 20.8575 12.396Z" fill="#009DED"/>
										</svg>
									</a>
								<?php } ?>

								<?php if( $linkedin_username ) { ?>
									<a href="https://www.linkedin.com/in/<?php echo $linkedin_username; ?>" target="_blank">
										<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M15 0.600037C7.04698 0.600037 0.599976 7.04704 0.599976 15C0.599976 22.953 7.04698 29.4 15 29.4C22.953 29.4 29.4 22.953 29.4 15C29.4 7.04704 22.953 0.600037 15 0.600037ZM11.475 20.9685H8.55898V11.5845H11.475V20.9685ZM9.99898 10.4325C9.07798 10.4325 8.48248 9.78004 8.48248 8.97304C8.48248 8.14954 9.09598 7.51654 10.0365 7.51654C10.977 7.51654 11.553 8.14954 11.571 8.97304C11.571 9.78004 10.977 10.4325 9.99898 10.4325ZM22.125 20.9685H19.209V15.768C19.209 14.5575 18.786 13.7355 17.7315 13.7355C16.926 13.7355 16.4475 14.292 16.236 14.8275C16.158 15.018 16.1385 15.288 16.1385 15.5565V20.967H13.221V14.577C13.221 13.4055 13.1835 12.426 13.1445 11.583H15.678L15.8115 12.8865H15.87C16.254 12.2745 17.1945 11.3715 18.768 11.3715C20.6865 11.3715 22.125 12.657 22.125 15.42V20.9685Z" fill="#0073B1"/>
										</svg>
									</a>
								<?php } ?>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	
	<?php if( is_front_page() ) {
		
		$frontpage_id = get_option( 'page_on_front' );
		?>

		<div class="hero-slider">
			<?php
				// Check rows exists.
				if( have_rows('field_619257f727d4d', $frontpage_id) ) {

					// Loop through rows.
					while( have_rows('field_619257f727d4d', $frontpage_id) ) { the_row();
						// Load sub field value.
						$hero_bg = get_sub_field('field_6192580927d4e');
						$heading = get_sub_field('field_6192581127d4f');
						$subheading = get_sub_field('field_6192582327d50');
						?>
							<div>
								<figure class="mb-0">
									<img class="object-cover w-full" src="<?php echo esc_url( $hero_bg['url'] ); ?>" alt="">
								</figure>

								<div class="absolute left-0 w-full slider-content">
									<div class="container">
										<div class="row">
											<div class="offset-lg-2 col-lg-8 text-white text-uppercase text-center">
												<h3 class="relative flex align-items-center justify-content-center text-3xl lg:text-4xl font-light">
													<span class="px-4"><?php echo $heading; ?></span>
												</h3>
												<h2 class="font-bold text-4xl lg:text-5xl"><?php echo $subheading ?></h2>
											</div>
										</div>
									</div>
								</div>
							
							</div>
						<?php
					}
					
				}
			?>
		</div>

	<?php } ?>

	<nav role="navigation" class="hidden lg:flex main-navigation">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					
					<?php wp_nav_menu( array( 
						'theme_location' => 'primary',
						'menu_id' 		 => 'main-menu',
						'menu_class' 	 => 'main-menu list-unstyled ml-0 flex justify-content-between',
						'container' 	 => 'ul'
					) ); ?>

				</div>
			</div>
		</div>
	</nav>

	<div id="content" class="site-content">
