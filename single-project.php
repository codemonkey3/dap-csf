<?php
/**
 * The template for displaying all single project posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package james-walker
 */

get_header();

// intro text
$intro_heading = get_field('field_61965e77d5e53');
$intro_excerpt = get_field('field_61965e86d5e55');
?>
	
	<main id="main" class="site-main" role="main">
    <div id="primary" class="content-area">

      <div class="hero-silder">
        <?php
          // Check rows exists.
          if( have_rows('field_61965e3df76fe') ):
            // Loop through rows.
            while( have_rows('field_61965e3df76fe') ) : the_row();

              // Load sub field value.
              $hero_bg = get_sub_field('field_61965e4bf76ff');
              ?>
                <div>
                  <img class="object-fit w-full" src="<?php echo $hero_bg['url']; ?>" alt="<?php echo $hero_bg['alt']; ?>">
                </div>
              <?php
            endwhile;
          endif;
        ?>
      </div>
      
      <div class="container">
        <div class="row">

          <div class="col-lg-12">
            <div class="bg-white intro-text">
              <h1 class="mb-4 font-bold text-3xl lg:text-4xl text-blue text-uppercase">
                <?php echo ("" != $intro_heading) ? $intro_heading : get_the_title(); ?>
              </h1>
              <article class="font-light">
                <?php echo wpautop( $intro_excerpt ); ?>
              </article>
            </div>
          </div>

        </div> <!-- .container -->
      </div>

      <?php
        // Check rows exists.
        if( have_rows('field_6196246682bfe') ) {

          // Loop through rows.
          while( have_rows('field_6196246682bfe') ) { the_row();
            $bg_color = get_sub_field('field_6196247782bff');
            $heading = get_sub_field('field_61965eb254b8b');
            $heading_color = get_sub_field('field_61a0f2dc8cd88');
            $text_color = get_sub_field('field_61a0f3088cd89');
            $content = get_sub_field('field_61965eb854b8c');
            ?>
              <section style="background-color: <?php echo $bg_color; ?>; color: <?php echo $text_color ? $text_color : '#000'; ?>">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12">

                      <div class="mb-10 text-center">
                        <h2 class="mb-5 font-bold text-xl lg:text-2xl"
                          style="color: <?php echo $heading_color; ?>">
                          <?php echo $heading; ?>
                        </h2>
                        <div class="font-light excerpt">
                          <?php echo wpautop( $content ); ?>
                        </div>
                      </div>

                    </div>
                  </div>

                  <?php
                    // Check rows exists.
                    if( have_rows('field_61965d8f82c02') ) {
                      // Loop through rows.
                      while( have_rows('field_61965d8f82c02') ) { the_row();
                        $media = get_sub_field('field_619610106f29c');
                        $media_alignment = get_sub_field('field_61965f467eaa2');
                        $heading = get_sub_field('field_619610246f29d');
                        $excerpt = get_sub_field('field_61961cd86f29e');
                        ?>
                          <div class="pb-12">
                            <div class="row <?php echo 'right' == $media_alignment ? 'flex-row-reverse' : ''; ?> align-items-center">
                              <div class="col-lg-6">
                                <figure class="mb-6 lg:mb-0">
                                  <img src="<?php echo esc_url( $media['url'] ); ?>" alt="<?php echo esc_url( $media['alt'] ); ?>">
                                </figure>
                              </div>
                              <div class="col-lg-6">
                                <h3 class="mb-4 font-bold text-lg lg:text-xl"
                                  style="color: <?php echo $heading_color; ?>">
                                  <?php echo $heading; ?>
                                </h3>
                                <article class="font-light">
                                  <?php echo wpautop( $excerpt ); ?>
                                </article>

                                <?php
                                  // Check rows exists.
                                  if( have_rows('field_61961cfa6f29f') ) {
                                    // Loop through rows.
                                    while( have_rows('field_61961cfa6f29f') ) { the_row();
                                      $cta = get_sub_field('field_61961d996f2a0');
                                      $cta_layout = get_sub_field('field_61961da56f2a1');
                                      ?>
                                        <a class="mb-5 btn btn-<?php echo $cta_layout; ?> <?php echo 'ghost' == $cta_layout ? '' : 'text-black'; ?> block " href="<?php echo esc_url( $cta['url'] ); ?>">
                                          <?php echo esc_attr( $cta['title'] ); ?>
                                        </a>
                                      <?php
                                    }
                                  }
                                ?>
                              </div>
                            </div>
                          </div>
                        <?php
                      }
                    }
                  ?>
                </div>
              </section>
            <?php
          }
        }
      ?>

    </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();