<?php
/**
 * dap-csf functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package dap-csf
 */

if ( ! function_exists( 'dap_csf_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function dap_csf_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on dap-csf, use a find and replace
	 * to change 'dap-csf' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'dap-csf', get_template_directory() . '/languages' );
	
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'top_menu' 		=> esc_html__( 'Top Menu', 'dap-csf' ),
		'primary' 		=> esc_html__( 'Main Menu', 'dap-csf' ),
		'mobile_menu' 		=> esc_html__( 'Mobile Menu', 'dap-csf' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'width'       => 120,
		'height'      => 320,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'dap_csf_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dap_csf_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'dap_csf_content_width', 640 );
}
add_action( 'after_setup_theme', 'dap_csf_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dap_csf_widgets_init() {

	$sidebars = array(
		'sidebar-1'	=> 'Sidebar'
	);

	foreach( $sidebars as $id => $name ) {
		register_sidebar( array(
			'name'          => esc_html__( $name, 'dap-csf' ),
			'id'            => $id,
			'description'   => '',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}
}
add_action( 'widgets_init', 'dap_csf_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function dap_csf_scripts() {	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'dap_csf_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Shortocde file for this theme.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Load AJAX file for this theme.
 */
require get_template_directory() . '/inc/ajax.php';

/**
 * Load Actions file for this theme.
 */
require get_template_directory() . '/inc/actions.php';

/**
 * Load available Widgets for this theme.
 */
require get_template_directory() . '/inc/user-api.php';

/**
 * Load Filters file for this theme.
 */
require get_template_directory() . '/inc/filters.php';

/**
 * Load Filters file for this theme.
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Load theme functions
 */
require get_template_directory() . '/inc/dap-csf-functions.php';

/**
 * Load available Widgets for this theme.
 */
require get_template_directory() . '/lib/widgets.php';