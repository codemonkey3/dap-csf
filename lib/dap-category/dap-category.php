<?php

class DAP_CSF_Category extends WP_Widget {
	
	/**
	 * Constructor. Set the default widget options and create widget.
	 *
	 * @since 0.1.8
	 */
	public function __construct() {
		parent::__construct(
			'dap-csf-category',
			esc_html__( 'DAP CSF: Category', 'dap-csf' ), 
			[
				'classname'   => 'dap-csf-category',
				'description' => __( 'Displays DAP-CSF Category.', 'dap-csf' ),
			]
		);

		add_action( 'wp_enqueue_scripts', [ $this, 'dap_csf_enqueue_assets' ] );
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$curent_term_id = 0;
		
		// check if we're in category page
		if( is_category() ) {
			global $wp_query;

    	$curent_term_id = $wp_query->get_queried_object_id();
		}

		echo $args['before_widget'];
		?>

			<?php
				if ( ! empty( $instance['title'] ) ) {
					echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
				}
			?>

			<div class="widget_categories">
				<?php 
          // get excluded IDs
          $exclude_ids = $instance['exclude_ids'];
          
          $category_args = [
            'taxonomy' => 'category',
            'hide_empty' => false,
						'parent'   => 0
          ];
					
          if( $exclude_ids ) {
            $category_args['exclude'] = [$exclude_ids];
          }
          
          // get terms
          $terms = get_terms( $category_args );
          
          if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
            ?>
            <ul class="post-categories">
              <?php foreach( $terms as $term ) {
								
								$subcategory_args = [
									'taxonomy' => 'category',
									'hide_empty' => false,
									'parent' => $term->term_id
								];

								// get terms
								$sub_terms = get_terms( $subcategory_args );
								?>
                <li>
                  <a href="javascript: void(0);" 
                    alt="<?php echo esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ); ?>">
                    									
										<div class="flex justify-content-between align-items-center">
											<span><?php echo $term->name; ?></span>
											
											<?php if ( ! empty( $sub_terms ) && ! is_wp_error( $sub_terms ) ) { ?>
												<div style="width: 15px;">
													<svg width="15" height="8" viewBox="0 0 15 8" fill="none" xmlns="http://www.w3.org/2000/svg">
														<path d="M1 1L7.25 6.27483L13.5 1" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
													</svg>
												</div>
											<?php } ?>
										</div>										
                  </a>
									
									<?php
										if ( ! empty( $sub_terms ) && ! is_wp_error( $sub_terms ) ) {
											?>
											<ul style="<?php echo $sub_term->term_id == $curent_term_id ? '' : 'display: none;' ?>">
												<?php
													foreach( $sub_terms as $sub_term ) {
														?>
															<li class="<?php echo $sub_term->term_id == $curent_term_id ? 'current-item' : '' ?>">
																<a href="<?php echo esc_url( get_term_link( $sub_term ) ); ?>">
																	<div class="media align-items-center">
																		<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
																			<path d="M8.6543 17.7653L14.4235 12.0509L8.6543 6.33652" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
																		</svg>
																		<span><?php echo $sub_term->name; ?></span>
																	</div>
																</a>
															</li>
														<?php
													}
												?>
											</ul>
											<?php
										}
									?>
									
                </li>
              <?php } ?>
            </ul>
            <?php
          }
        ?>
			</div>
		
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Categories', 'swoop' );
		$exclude_ids = ! empty( $instance['exclude_ids'] ) ? $instance['exclude_ids'] : '';
		?>

		<div class="widget-body">
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title', 'text_domain' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'exclude_ids' ); ?>"><?php _e( 'Exclude IDs', 'swoop' ); ?>:</label>
				<textarea id="<?php echo $this->get_field_id( 'exclude_ids' ); ?>" name="<?php echo $this->get_field_name( 'exclude_ids' ); ?>" rows="3" class="widefat"><?php echo $exclude_ids; ?></textarea>
        <small>separate with comma</small>
			</p>
		</div>

		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['exclude_ids'] = ( ! empty( $new_instance['exclude_ids'] ) ) ? $new_instance['exclude_ids'] : '';
		
		return $instance;
	}
	
	public function dap_csf_enqueue_assets() {
		if( is_active_widget( false, false, $this->id_base, true ) ) {
			wp_enqueue_style('dap-csf-category-style', get_stylesheet_directory_uri() . '/lib/dap-category/dap-category.css', [], ['1.0'], 'all');
		}
	}
	
}