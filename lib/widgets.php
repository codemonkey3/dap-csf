<?php
	
add_action( 'widgets_init', 'dap_csf_load_widgets' );
/*
 * register thumbnail list widget
 */
function dap_csf_load_widgets() {
	include_once( get_stylesheet_directory() . '/lib/dap-category/dap-category.php' );
	
	register_widget( 'DAP_CSF_Category' );
}