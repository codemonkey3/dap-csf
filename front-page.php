<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dap-csf
 */

get_header();

// get site settings 
$options = get_option( 'dap_csf_theme_options' );

// get ACF value
// Center for Strategic Futures
$strategic_bg_color = get_field('field_6195bf95d3318');
$strategic_bg_image  = get_field('field_619258b9d87c2');
$strategic_heading = get_field('field_61925877d87bf');
$strategic_content = get_field('field_61925899d87c0');
$strategic_cta = get_field('field_619258add87c1');

// Products & Services
$product_bg_color = get_field('field_6195ba36f8512');
$product_heading = get_field('field_619258d1d87c4');
$product_excerpt = get_field('field_619258e1d87c5');

// News
$blog_heading = get_field('field_6195b459011aa');
$blog_featured_id = get_field('field_61925955f209e');
?>
	
	<main id="main" class="site-main" role="main">
		
		<div id="primary" class="content-area">
			
			<style>
				@media (min-width: 768px) {
					.strategic-future .inner {
						background-image: url(<?php echo $strategic_bg_image['url'] ?>);
						background-position: center;
						background-repeat: no-repeat;
					}
				}
			</style>
			<div class="mt-12 lg:mt-0 strategic-future">
				<div class="container">
					
					<div class="py-2 lg:py-8 inner"
						style="background-color: <?php echo $strategic_bg_color; ?>;">
					<div class="row">
						<div class="offset-lg-2 col-lg-8">

								<div class="p-6 lg:p-0 text-center">
									<h2 class="mb-5 font-bold text-2xl"><?php echo $strategic_heading; ?></h2>
									<article class="font-light text-justify">
										<?php echo wpautop( $strategic_content ); ?>
									</article>

									<?php if( $strategic_cta && "" != $strategic_cta['url'] ) { ?>
										<a class="btn bg-blue lg:bg-yellow" href="<?php echo esc_url( $strategic_cta['url'] ); ?>">
											<?php echo esc_attr( $strategic_cta['title'] ); ?>
										</a>
									<?php } ?>
								</div>

							</div>

						</div>
					</div>
				</div> <!-- .container -->
			</div>

			<div class="product-services text-white"
				style="background: <?php echo $product_bg_color; ?>">
				<div class="container">
					
					<div class="row">
						<div class="offset-lg-2 col-lg-8">

							<div class="mb-14 text-center">
								<h2 class="mb-4 font-bold text-2xl"><?php echo $product_heading; ?></h2>
								<article class="font-light">
									<?php echo wpautop( $product_excerpt ); ?>
								</article>
							</div>

						</div>
					</div>
					
					<div class="row">
						<?php
							// Check rows exists.
							if( have_rows('field_619258f2d87c6') ):

								// Loop through rows.
								while( have_rows('field_619258f2d87c6') ) : the_row();

										// Load sub field value.
										$thumbnail = get_sub_field('field_61925901d87c7');
										$title = get_sub_field('field_61925905d87c8');
										$cta = get_sub_field('field_61950f4602540');
										?>
										<div class="col-lg-4">

											<div class="mb-8 lg:mb-0">
												<?php if( $cta ) { ?>
													<a class="text-white" href="<?php echo esc_url($cta['url']); ?>">
												<?php } ?>
												
												<div class="text-center item">
													<figure class="mb-3 lg:mb-8">
														<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
													</figure>
													
													<h3 class="mb-0 font-bold text-xl">
														<?php echo $title; ?>
													</h3>
												</div>

												<?php if( $cta ) { ?>
													</a>
												<?php } ?>
											</div>
											
										</div>
										<?php
								// End loop.
								endwhile;

							// No value.
							else :
								// Do something...
							endif;
						?>
					</div>
					
				</div>
			</div>

			<div class="news">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							
							<h2 class="mb-8 font-bold text-2xl text-center">
								<?php echo $blog_heading; ?>
							</h2>
							
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6">
							<?php
								$arg_featured = [
									'posts_per_page' => 1
								];

								if( $blog_featured_id ) {
									$arg_featured['p'] = $blog_featured_id;
								}

								// The Query
								$query_featured = new WP_Query( $arg_featured );
								
								// The Loop
								if ( $query_featured->have_posts() ) {
									while ( $query_featured->have_posts() ) { $query_featured->the_post();
										?>
										
										<div class="bg-white blog-card">
											<figure class="mb-6">
												<?php 
													if ( has_post_thumbnail() ) :
														the_post_thumbnail('large', ['class' => 'object-cover w-full']);
													endif;
												?>
											</figure>

											<h3 class="mb-0 font-bold text-base">
												<?php the_title(); ?>
											</h3>
											
											<time class="mb-4 font-light text-xs block" datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
												<?php echo get_the_date(); ?>
											</time>
											
											<div class="text-sm excerpt">
												<p>
													<?php echo dap_csf_excerpt( wp_strip_all_tags( get_the_content() ), '120' ); ?>
													<a href="<?php the_permalink(); ?>" class="read-more">Read more...</a>
												</p>
											</div>
										</div>
										<?php
									}
								} 
								else {
									// no posts found
								}

								/* Restore original Post Data */
								wp_reset_postdata();
							?>
						</div>
						<div class="col-lg-6">
							<?php
								$args = [
									'posts_per_page' => 3,
								];

								if( $blog_featured_id ) {
									$args['post__not_in'] = [$blog_featured_id];
								}
								else {
									$args['offset'] = 1;
								}
								
								// The Query
								$the_query = new WP_Query( $args );
								$post_count = $the_query->post_count;
								
								// The Loop
								if ( $the_query->have_posts() ) {

									// item counter
									$item_counter = 1;

									while ( $the_query->have_posts() ) { $the_query->the_post();
										?>
										<div class="bg-white blog-card sm <?php echo $post_count != $item_counter ? 'mb-4' : ''; ?>">
											<div class="row">
												<div class="col-lg-5">
													<figure class="lg:mb-0">
														<?php 
															if ( has_post_thumbnail() ) :
																the_post_thumbnail('large', ['class' => 'object-cover w-full']);
															endif;
														?>
													</figure>
												</div>
												<div class="col-lg-7">
													<h3 class="mb-0 font-bold text-sm">
														<?php the_title(); ?>
													</h3>
													
													<time class="mb-4 font-light text-tiny block" datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
														Posted on <?php echo get_the_date(); ?>
													</time>
													
													<div class="text-xs excerpt">
														<?php the_excerpt(); ?>
													</div>
												</div>
											</div>
										</div>
										<?php

										// increment counter
										$item_counter++;
									}
								} 
								else {
									// no posts found
								}
								
								/* Restore original Post Data */
								wp_reset_postdata();
							?>
						</div>
					</div>
					
					<div class="mt-4 mb-2 row">
						<div class="col-lg-12">
							<a class="fill:yellow" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
								<div class="media align-items-center justify-content-end">
									<span>See All</span>								
									<svg width="25" height="22" viewBox="0 0 25 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M9.375 16.4634L15.625 10.9756L9.375 5.48779" stroke="#1B2C94" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg>
								</div>
							</a>
						</div>
					</div>

				</div>
			</div>

		</div><!-- #primary -->
	</main><!-- #main -->
		
<?php
get_footer();