<form role="search" action="<?php echo get_the_permalink(69) ?>" method="GET">
  <p class="mb-0">
    <label class="screen-reader-text" for="s">Search</label>
    <input type="search" 
      name="q" 
      id="q" 
      class="has-icon align-right-icon search-icon" 
      value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>"
      placeholder="Search"
    >
  </p>
</form>